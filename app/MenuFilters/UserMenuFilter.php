<?php
namespace App\MenuFilters;
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-11-13 17:28:13
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2019-01-07 12:49:50
 */

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class UserMenuFilter implements FilterInterface
{
    private $canAccess = 'user';

    public function transform($item, Builder $builder)
    {
        if(\Auth::user()->user_type == $this->canAccess)
    	{
	        if (isset($item['can_access']) && is_array($item['can_access']) && in_array($this->canAccess, $item['can_access'])) 
	        {
                if(!isset($item['show_when']) || (\Auth::user()->gamePlays->count() >= $item['show_when']))
	               return $item;
	        }
	        return false;
	    }
        return $item;
    }
}