<?php
namespace MenuFilter;
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-11-13 17:28:13
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-11-13 17:28:53
 */

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class SuperAdminMenuFilter implements FilterInterface
{
    public function transform($item, Builder $builder)
    {
        if (isset($item['permission']) && ! Laratrust::can($item['permission'])) {
            return false;
        }

        return $item;
    }
}