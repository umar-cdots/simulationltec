<?php
namespace App\MenuFilters;
/**
 * @Author: Muhammad Umar Hayat
 * @Date:   2018-11-13 17:28:13
 * @Last Modified by:   Muhammad Umar Hayat
 * @Last Modified time: 2018-11-13 17:49:41
 */

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class AdminMenuFilter implements FilterInterface
{
    private $canAccess = 'admin';

    public function transform($item, Builder $builder)
    {
        if(\Auth::user()->user_type == $this->canAccess)
    	{
	        if (isset($item['can_access']) && is_array($item['can_access']) && in_array($this->canAccess, $item['can_access'])) 
	        {
	            return $item;
	        }
	        return false;
	    }
        return $item;
    }
}