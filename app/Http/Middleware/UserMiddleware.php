<?php

namespace App\Http\Middleware;

use Closure;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->user_type != 'user')
        {
            return back();
        }
        elseif(empty(\Auth::user()->nick_name) && \Auth::user()->user_type == 'user')
        {
            return redirect()->route('user.edit', \Auth::user()->id)->with(['response' => ['status' => "success", 'message' => "Please Complete Your Profile First."]]);
        }

        return $next($request);
    }
}
