<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sessions;
use App\Project;
use Validator;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('session.index')->with(['sessions' => Sessions::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('session.create')->with(['projects' => Project::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'project_id.required' => 'Project is Required',
            'project_id.exist'    => 'Project Doesn\'t Exist',
            'title.required'      => 'Session Title is Required',
          ];
        Validator::make($request->all(), [
            'project_id'              => 'required|exists:projects,id',
            'title'                   => 'required|max:255',
        ], $messages)->validate();

        $session = new Sessions;
        $session->project_id = $request->project_id;
        $session->title      = $request->title;
        $session->save();

        return redirect()->route('session.index')->with(['response' => ['status' => "success", 'message' => "Session created successfully."]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $session = Sessions::findOrFail($id);

        return view('session.edit')->with(['session' => $session, 'projects' => Project::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $session = Sessions::findOrFail($id);

        $messages = [
            'project_id.required' => 'Project is Required',
            'project_id.exist'    => 'Project Doesn\'t Exist',
            'title.required'      => 'Session Title is Required',
          ];
        Validator::make($request->all(), [
            'project_id'              => 'required|exists:projects,id',
            'title'                   => 'required|max:255',
        ], $messages)->validate();

        $session->project_id = $request->project_id;
        $session->title      = $request->title;
        $session->save();

        return redirect()->route('session.index')->with(['response' => ['status' => "success", 'message' => "Session updated successfully."]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $session = Sessions::findOrFail($id);
        $session->delete();

        return redirect()->route('session.index')->with(['response' => ['status' => "success", 'message' => "Session removed successfully."]]);
    }
}
