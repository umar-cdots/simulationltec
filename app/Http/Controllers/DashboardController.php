<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
	public function index()
	{
		if(\Auth::user()->user_type == 'user')
		{
			return redirect()->route('simulator');
		}
		return redirect()->route('user.index');
	}
}
