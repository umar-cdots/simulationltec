<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Project;
use App\Sessions;
use App\PersonalityType;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index', ['users' => User::where('user_type', 'user')->session(request('session_id'))->get(), 'sessions' => Sessions::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create', [/*'projects' => Project::all(),*/ 'sessions' => Sessions::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            // 'project_id.required' => 'Project is Required',
            // 'project_id.exist'    => 'Project Doesn\'t Exist',
            'session_id.required' => 'Session is Required',
            'session_id.exist'    => 'Session Doesn\'t Exist',
            'name.required'       => 'Name is Required',
            'name.max'            => 'Name is too long',
            'email.required'      => 'Email is Required',
            'email.email'         => 'Email Format is Invalid',
            'email.unique'        => 'Email Already Exists',
            'password.required'   => 'Password is Required',
            'password.min'        => 'Password Cannot be Shorter Than 6',
            'password.confirmed'  => 'Password & Confirm Password Doesn\'t Match'
          ];
        Validator::make($request->all(), [
            // 'project_id'              => 'required|exists:projects,id',
            'session_id'              => 'required|exists:sessions,id',
            'name'                    => 'required|max:255',
            'email'                   => 'required|email|unique:users,email',
            'password'                => 'required|min:6|confirmed',
        ], $messages)->validate();

        $user               = new User;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->password     = bcrypt($request->password);
        $user->session_id   = $request->session_id;
        $user->user_type    = "user";
        $user->save();

        return redirect()->route('user.index')->with(['response' => ['status' => "success", 'message' => "User created successfully."]]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('user.show', ['user' => User::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!\Auth::id() != $id && !\Auth::user()->user_type == 'super_admin')
            abort(403);

        return view('user.edit', ['user' => User::findOrFail($id), 'sessions' => Sessions::all(), 'personalityTypes' => PersonalityType::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!\Auth::id() != $id && !\Auth::user()->user_type == 'super_admin')
            abort(403);

        $user = User::findOrFail($id);

        $messages = [
            'nick_name.required'       => 'Nickname is Required',
            'nick_name.max'            => 'Nickname is too long',
            'personality_type_id.exists' => 'Personality Type Doesn\'t Exists',
            'gender'                  => 'Invalid Gender',
          ];
        Validator::make($request->all(), [
            'nick_name'                    => 'required|max:255',
            'personality_type_id'         => 'sometimes|exists:personality_types,id',
            'gender'                      => 'sometimes|in:male,female,undefined',
        ], $messages)->validate();
        
        $user->nick_name            = $request->nick_name;
        $user->personality_type_id  = $request->personality_type_id;
        $user->gender               = $request->gender;
        $user->education_level      = $request->education_level;
        $user->organization_type    = $request->organization_type;
        $user->organization_size    = $request->organization_size;
        $user->industry             = json_encode($request->industry);
        $user->function             = json_encode($request->function);
        $user->career_level         = $request->career_level;
        $user->working_experience   = $request->working_experience;
        $user->save();

        return redirect()->route(\Auth::user()->user_type == 'super_admin' ? 'user.index' : 'simulation')->with(['response' => ['status' => "success", 'message' => "User created successfully."]]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('user.index')->with(['response' => ['status' => "success", 'message' => "User removed successfully."]]);
    }
}
