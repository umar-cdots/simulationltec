<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\GamePlay;
use App\GameStage;
use App\GameStagewiseData;
use App\GameStagewiseEngagement;
use App\Leader;
use App\PersonalityType;
use App\GameCharacter;
use App\PersonalShift;
use Auth;

class ReportsController extends Controller
{
    private $FirstPartOfSummary = array();
    private $SecondPartOfSummary = array();

    public function __construct()
    {
        $this->FirstPartOfSummary   = ['66' => "You have interpreted the behaviors of this style successfully", '33' => "You have been uncertain in interpreting the behaviors of this style", '0' => "You haven't interpreted correctly the behaviors of this style"];
        $this->SecondPartOfSummary  = ['66' => "you have flexed the way you engage with people effectively.", '33' => "you haven't been always effective in engaging people.", '0' => "you have been ineffective in engaging people."];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function performanceAnalysis($game_play_id)
    {
        if(!\Auth::user()->gamePlays->count() >= 1)
        {
            return abort(403);
        }

        $game_play      = GamePlay::findOrFail($game_play_id);

        if($game_play->user_id != \Auth::id())
        {
            return abort(401);
        }

        $project        = $game_play->user->session->project;
        $game_stage_ids = GameStage::where('game_play_id', $game_play_id)
                                        ->orderBy('id', 'ASC')
                                            ->get()
                                                ->pluck('id');
        $stages         = $game_stage_ids;
        // $stages->shift();

        $total_decisions_made           = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where('engaged_after_observing', true)->count();
        $decisions_made_after_observing = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where(['engaged_after_observing' => true, 'is_observed' => true])->count();
        $decisions_made_without_observing = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where(['engaged_after_observing' => true, 'is_observed' => false])->count();

        $no_of_potential_diagnose       = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where('personality_type_id', '<>', 0)->count();
        $no_of_potential_engagements    = GameStagewiseEngagement::whereIn('game_stage_id', $stages->all())->count();

        $leaders        = Leader::where('project_id', $project->id)->get();

        $game_data      = GameStagewiseData::whereIn('game_stage_id', $stages->all())->orderBy('id', 'ASC')->get();

        $personality_types = PersonalityType::all();

        $specific_profiles = [];
        
        $game_characters   = $game_play->gameCharacter;

        foreach($personality_types as $personality_type)
        {
            $true_positive     = 0;
            $true_negative     = 0;
            $false_positive    = 0;
            $false_negative    = 0;
            $data              = $game_data->groupBy('leader_id');
            foreach($data as $leader_id => $data_row)
            {
                foreach($data_row as $row)
                {
                    $game_character = GameCharacter::where(['game_play_id' => $game_play->id, 'leader_id' => $row->leader_id])->first();

                    if($personality_type->id == $game_character->personality_type_id && $personality_type->id == $row->personality_type_id)
                    {
                        $true_positive++;
                    }

                    if($personality_type->id != $game_character->personality_type_id && $personality_type->id == $row->personality_type_id)
                    {
                        $false_positive++;
                    }

                    if($personality_type->id != $game_character->personality_type_id && $personality_type->id != $row->personality_type_id)
                    {
                        $true_negative++;
                    }

                    if($personality_type->id == $game_character->personality_type_id && $game_character->personality_type_id != $row->personality_type_id)
                    {
                        $false_negative++;
                    }
                }
            }
            $specific_profiles[$personality_type->short_title]   = ['true_positive' => $true_positive, 'false_positive' => $false_positive, 'true_negative' => $true_negative, 'false_negative' => $false_negative, 'correct_engagements' => 0, 'total_engagements' => 0];
        }

        $game_characters   = GameCharacter::where(['game_play_id' => $game_play->id])->get();

        foreach($game_characters as $game_character)
        {
            $short_title = PersonalityType::find($game_character->personality_type_id)->short_title;
            $specific_profiles[$short_title]['correct_engagements'] += $game_character->leader->gameStagewiseEngagements()->whereIn('game_stage_id', $stages->all())->where(['accuracy' => 'correct'])->count();
            $specific_profiles[$short_title]['total_engagements'] += 28;
        }

        //All The Calculations
        $per_descisions_made        = round($decisions_made_after_observing / ($total_decisions_made == 0 ? 1 : $total_decisions_made) * 100);
        $per_potential_diagnosis    = round($no_of_potential_diagnose / 35 * 100);
        $per_potential_engagements  = round($no_of_potential_engagements / 140 * 100);
        $per_approach_quality       = round(($per_descisions_made+$per_potential_diagnosis+$per_potential_engagements) / 3);

        $per_understanding_others   = 0;
        $per_adapting_to_others     = 0;
        foreach($leaders as $leader)
        {
            $diagnose   = $leader->gameCharacter()->where('game_play_id', $game_play->id)->first()->personalityType;
            $correct_diagnose   = 0;
            $diagnose_count     = 0;
            $correct_engagements_count  = 0;
            $diagnose_count     = 0;
            foreach($game_data->where('leader_id', $leader->id)->all() as $data)
            {
                if(isset($data->personalityType->short_title))
                {
                    if($data->personalityType->short_title == $diagnose->short_title)
                    {
                        $correct_diagnose++;
                    }
                }
                $diagnose_count++;
            }
            $per_understanding_others += round($correct_diagnose / 7 * 100);
            foreach($stages as $stage)
            {
                $correct_engagements    = $leader->gameStagewiseEngagements()->where(['game_stage_id' => $stage, 'leader_id' => $leader->id, 'accuracy' => 'correct'])->count();
                $correct_engagements_count += $correct_engagements;
            }
            $per_adapting_to_others += round($correct_engagements_count / 28 * 100);
        }
        $per_understanding_others = round($per_understanding_others / $leaders->count());
        $per_adapting_to_others   = round($per_adapting_to_others / $game_characters->count());

        return view('reports.performance_analysis')->with(['project' => $project, 'no_of_potential_diagnose' => $no_of_potential_diagnose, 'no_of_potential_engagements' => $no_of_potential_engagements, 'leaders' => $leaders, 'game_play' => $game_play, 'game_data' => $game_data, 'stages' => $stages, 'total_decisions_made' => $total_decisions_made, 'decisions_made_after_observing' => $decisions_made_after_observing, 'decisions_made_without_observing' => $decisions_made_without_observing, 'personality_types' => $personality_types, 'specific_profiles' => $specific_profiles, 'per_descisions_made' => $per_descisions_made, 'per_potential_diagnosis' => $per_potential_diagnosis, 'per_potential_engagements' => $per_potential_engagements, 'per_approach_quality' => $per_approach_quality, 'per_understanding_others' => $per_understanding_others, 'per_adapting_to_others' => $per_adapting_to_others, 'decisions_made_after_observing' => $decisions_made_after_observing, 'first_part_of_summary' => $this->FirstPartOfSummary, 'second_part_of_summary' => $this->SecondPartOfSummary]);
    }

    public function comparativeAnalysis()
    {
        if(!(\Auth::user()->gamePlays->count() >= 2))
        {
            return abort(403);
        }

        $game_plays         = GamePlay::where('user_id', Auth::id())->get();
        $project            = $game_plays[0]->user->session->project;
        $leaders            = Leader::where('project_id', $project->id)->get();
        $personality_types  = PersonalityType::all();

        $per_approach_quality_arr       = [];
        $per_understanding_others_arr   = [];
        $per_adapting_to_others_arr     = [];

        $per_descisions_made_arr        = [];
        $per_potential_diagnosis_arr    = [];
        $per_potential_engagements_arr  = []; 

        $per_performance_arr            = [];

        foreach($game_plays as $index => $game_play)
        {
            $game_stage_ids = GameStage::where('game_play_id', $game_play->id)
                                            ->orderBy('id', 'ASC')
                                                ->get()
                                                    ->pluck('id');
            $stages         = $game_stage_ids;
            $stages->shift();

            $total_decisions_made           = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where('engaged_after_observing', true)->count();
            $decisions_made_after_observing = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where(['engaged_after_observing' => true, 'is_observed' => true])->count();
            $decisions_made_without_observing = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where(['engaged_after_observing' => true, 'is_observed' => false])->count();

            $no_of_potential_diagnose       = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where('personality_type_id', '<>', 0)->count();
            $no_of_potential_engagements    = GameStagewiseEngagement::whereIn('game_stage_id', $stages->all())->count();

            $game_data      = GameStagewiseData::whereIn('game_stage_id', $stages->all())->orderBy('id', 'ASC')->get();

            $specific_profiles = [];
            
            $game_characters   = $game_play->gameCharacter;

            foreach($personality_types as $personality_type)
            {
                $true_positive     = 0;
                $true_negative     = 0;
                $false_positive    = 0;
                $false_negative    = 0;
                $data              = $game_data->groupBy('leader_id');
                foreach($data as $leader_id => $data_row)
                {
                    foreach($data_row as $row)
                    {
                        $game_character = GameCharacter::where(['game_play_id' => $game_play->id, 'leader_id' => $row->leader_id])->first();

                        if($personality_type->id == $game_character->personality_type_id && $personality_type->id == $row->personality_type_id)
                        {
                            $true_positive++;
                        }

                        if($personality_type->id != $game_character->personality_type_id && $personality_type->id == $row->personality_type_id)
                        {
                            $false_positive++;
                        }

                        if($personality_type->id != $game_character->personality_type_id && $personality_type->id != $row->personality_type_id)
                        {
                            $true_negative++;
                        }

                        if($personality_type->id == $game_character->personality_type_id && $game_character->personality_type_id != $row->personality_type_id)
                        {
                            $false_negative++;
                        }
                    }
                }
                $specific_profiles[$personality_type->short_title]   = ['true_positive' => $true_positive, 'false_positive' => $false_positive, 'true_negative' => $true_negative, 'false_negative' => $false_negative, 'correct_engagements' => 0, 'total_engagements' => 0];

                $divider    = $true_positive + $false_negative;

                if($divider == 0)
                {
                    $divider = 1;
                }

                $sensitivity = round($true_positive / $divider * 100);

                $divider    = $true_negative + $false_positive;

                if($divider == 0)
                {
                    $divider = 1;
                }

                $specificty = round($true_negative / $divider * 100);

                $correct_engagements    = 0;
                $total_engagements      = 0;
                foreach($game_characters->where('personality_type_id', $personality_type->id)->all() as $game_character)
                {
                    $correct_engagements += $game_character->leader->gameStagewiseEngagements()->whereIn('game_stage_id', $stages->all())->where(['accuracy' => 'correct'])->count();
                    $total_engagements += 28;
                }

                $correct_engagements = round($correct_engagements / $total_engagements * 100);

                if(!isset($per_performance_arr[$personality_type->short_title]))
                {
                    $per_performance_arr[$personality_type->short_title] = [];
                }

                $per_performance_arr[$personality_type->short_title][] = round(($sensitivity + $specificty + $correct_engagements) / 3);
            }

            //All The Calculations
            $per_descisions_made        = round($decisions_made_after_observing / ($total_decisions_made == 0 ? 1 : $total_decisions_made) * 100);
            $per_potential_diagnosis    = round($no_of_potential_diagnose / 35 * 100);
            $per_potential_engagements  = round($no_of_potential_engagements / 140 * 100);
            $per_approach_quality       = round(($per_descisions_made+$per_potential_diagnosis+$per_potential_engagements) / 3);

            $per_approach_quality_arr[] = $per_approach_quality;
            $per_descisions_made_arr[]          = $per_descisions_made;
            $per_potential_diagnosis_arr[]      = $per_potential_diagnosis;
            $per_potential_engagements_arr[]    = $per_potential_engagements; 

            $per_understanding_others   = 0;
            $per_adapting_to_others     = 0;
            foreach($leaders as $leader)
            {
                $diagnose   = $leader->gameCharacter()->where('game_play_id', $game_play->id)->first()->personalityType;
                $correct_diagnose   = 0;
                $diagnose_count     = 0;
                $correct_engagements_count  = 0;
                $diagnose_count     = 0;
                foreach($game_data->where('leader_id', $leader->id)->all() as $data)
                {
                    if(isset($data->personalityType->short_title))
                    {
                        if($data->personalityType->short_title == $diagnose->short_title)
                        {
                            $correct_diagnose++;
                        }
                    }
                    $diagnose_count++;
                }
                $per_understanding_others += round($correct_diagnose / 7 * 100);
                foreach($stages as $stage)
                {
                    $correct_engagements    = $leader->gameStagewiseEngagements()->where(['game_stage_id' => $stage, 'leader_id' => $leader->id, 'accuracy' => 'correct'])->count();
                    $correct_engagements_count += $correct_engagements;
                }
                $per_adapting_to_others += round($correct_engagements_count / 28 * 100);
            }
            $per_understanding_others = round($per_understanding_others / $leaders->count());
            $per_adapting_to_others   = round($per_adapting_to_others / $game_characters->count());

            $per_understanding_others_arr[] = $per_understanding_others;
            $per_adapting_to_others_arr[]   = $per_adapting_to_others;
        }

        return view('reports.comparative_analysis')->with(['project' => $project, 'game_plays' => $game_plays, 'per_descisions_made' => $per_descisions_made, 'per_potential_diagnosis' => $per_potential_diagnosis, 'per_potential_engagements' => $per_potential_engagements, 'per_approach_quality' => $per_approach_quality, 'per_understanding_others' => $per_understanding_others, 'per_adapting_to_others' => $per_adapting_to_others, 'per_approach_quality_arr' => $per_approach_quality_arr, 'per_understanding_others_arr' => $per_understanding_others_arr, 'per_adapting_to_others_arr' => $per_adapting_to_others_arr, 'per_descisions_made_arr' => $per_descisions_made_arr, 'per_potential_diagnosis_arr' => $per_potential_diagnosis_arr, 'per_potential_engagements_arr' => $per_potential_engagements_arr, 'per_performance_arr' => $per_performance_arr]);
    }

    public function personalShifts()
    {
        if(!(\Auth::user()->gamePlays->count() >= 1))
        {
            return abort(403);
        }

        $game_plays         = GamePlay::where('user_id', Auth::id())->get();
        $project            = $game_plays[0]->user->session->project;
        $leaders            = Leader::where('project_id', $project->id)->get();
        $personality_types  = PersonalityType::all();

        $per_performance_arr            = [];
        foreach($game_plays as $index => $game_play)
        {
            $game_stage_ids = GameStage::where('game_play_id', $game_play->id)
                                            ->orderBy('id', 'ASC')
                                                ->get()
                                                    ->pluck('id');
            $stages         = $game_stage_ids;
            $stages->shift();

            $game_data      = GameStagewiseData::whereIn('game_stage_id', $stages->all())->orderBy('id', 'ASC')->get();

            $specific_profiles = [];
            
            $game_characters   = $game_play->gameCharacter;

            foreach($personality_types as $personality_type)
            {
                $true_positive     = 0;
                $true_negative     = 0;
                $false_positive    = 0;
                $false_negative    = 0;
                $data              = $game_data->groupBy('leader_id');
                foreach($data as $leader_id => $data_row)
                {
                    foreach($data_row as $row)
                    {
                        $game_character = GameCharacter::where(['game_play_id' => $game_play->id, 'leader_id' => $row->leader_id])->first();

                        if($personality_type->id == $game_character->personality_type_id && $personality_type->id == $row->personality_type_id)
                        {
                            $true_positive++;
                        }

                        if($personality_type->id != $game_character->personality_type_id && $personality_type->id == $row->personality_type_id)
                        {
                            $false_positive++;
                        }

                        if($personality_type->id != $game_character->personality_type_id && $personality_type->id != $row->personality_type_id)
                        {
                            $true_negative++;
                        }

                        if($personality_type->id == $game_character->personality_type_id && $game_character->personality_type_id != $row->personality_type_id)
                        {
                            $false_negative++;
                        }
                    }
                }
                $specific_profiles[$personality_type->short_title]   = ['true_positive' => $true_positive, 'false_positive' => $false_positive, 'true_negative' => $true_negative, 'false_negative' => $false_negative, 'correct_engagements' => 0, 'total_engagements' => 0];

                $divider    = $true_positive + $false_negative;

                if($divider == 0)
                {
                    $divider = 1;
                }

                $sensitivity = round($true_positive / $divider * 100);

                $divider    = $true_negative + $false_positive;

                if($divider == 0)
                {
                    $divider = 1;
                }

                $specificty = round($true_negative / $divider * 100);

                $correct_engagements    = 0;
                $total_engagements      = 0;
                foreach($game_characters->where('personality_type_id', $personality_type->id)->all() as $game_character)
                {
                    $correct_engagements += $game_character->leader->gameStagewiseEngagements()->whereIn('game_stage_id', $stages->all())->where(['accuracy' => 'correct'])->count();
                    $total_engagements += 28;
                }

                $correct_engagements = round($correct_engagements / $total_engagements * 100);

                if(!isset($per_performance_arr[$personality_type->short_title]))
                {
                    $per_performance_arr[$personality_type->short_title] = [];
                }

                $per_performance_arr[$personality_type->short_title][] = round(($sensitivity + $specificty + $correct_engagements) / 3);
            }
        }
        $average        = 100;
        $total_average  = 0;
        $overall        = [];
        $lowest         = [];
        foreach($per_performance_arr as $key => $arr)
        {
            $temp_collect   = collect($arr);
            $total          = $temp_collect->count();
            if($temp_collect->sum() / $total < $average)
            {
                $average    = $temp_collect->sum() / $total;
                $lowest     = ['personality_type' => $key, 'average' => $average];
            }
            else
            {
                $average        = $temp_collect->sum() / $total;
            }
            $total_average += $average;
        }
        $total_average /= 4;

        if($total_average > 95)
        {
            $message = "You flexed your style with mastery during the simulation. Please find below a full list of behaviors to refine your leadership effectiveness, and feel free to choose an area of focus for your development based on your self awareness.";
            $personal_shifts = PersonalShift::orderBy('id')->get();
        }
        elseif($lowest['average'] < 60)
        {
            $message = "Considering that you mainly struggled with the " . $lowest['personality_type'] . " style, the following behaviors can be critical to maximize your leadership effectiveness.";
            $personal_shifts = PersonalShift::where('personality_type_id', PersonalityType::where('short_title', $lowest['personality_type'])->first()->id)->orderBy('id')->get();
        }
        elseif($lowest['average'] > 60 && $lowest['average'] < 95)
        {
            $message = "Even if you performed well, you still didn't master the " . $lowest['personality_type'] . " style. To maximize your effectivness as a leader, it's recommended to develop the following behaviors.";
            $personal_shifts = PersonalShift::where('personality_type_id', PersonalityType::where('short_title', $lowest['personality_type'])->first()->id)->orderBy('id')->get();
        }

        return view('reports.personal_shifts')->with(['project' => $project, 'lowest' => $lowest, 'message' => $message, 'personal_shifts' => $personal_shifts]);
    }

    public function leaderboard()
    {
        $users  = \App\User::where(['user_type' => 'user', 'session_id' => Auth::user()->session_id])->get();

        foreach($users as &$user)
        {
            $game_play      = GamePlay::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
            if(empty($game_play))
            {
                $user->score = 0;
                continue;
            }
            $project        = $game_play->user->session->project ?? 'N/A';
            $game_stage_ids = GameStage::where('game_play_id', $game_play->id)
                                        ->orderBy('id', 'ASC')
                                            ->get()->pluck('id');
            $stages         = $game_stage_ids;
            $stages->shift();

            $total_decisions_made           = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where('engaged_after_observing', true)->count();
            $decisions_made_after_observing = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where(['engaged_after_observing' => true, 'is_observed' => true])->count();
            $decisions_made_without_observing = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where(['engaged_after_observing' => true, 'is_observed' => false])->count();

            $no_of_potential_diagnose       = GameStagewiseData::whereIn('game_stage_id', $stages->all())->where('personality_type_id', '<>', 0)->count();
            $no_of_potential_engagements    = GameStagewiseEngagement::whereIn('game_stage_id', $stages->all())->count();

            $leaders        = Leader::where('project_id', $project->id)->get();

            $game_data      = GameStagewiseData::whereIn('game_stage_id', $stages->all())->orderBy('id', 'ASC')->get();

            $personality_types = PersonalityType::all();

            $specific_profiles = [];
            
            $game_characters   = $game_play->gameCharacter;

            foreach($personality_types as $personality_type)
            {
                $true_positive     = 0;
                $true_negative     = 0;
                $false_positive    = 0;
                $false_negative    = 0;
                $data              = $game_data->groupBy('leader_id');
                foreach($data as $leader_id => $data_row)
                {
                    foreach($data_row as $row)
                    {
                        $game_character = GameCharacter::where(['game_play_id' => $game_play->id, 'leader_id' => $row->leader_id])->first();

                        if($personality_type->id == $game_character->personality_type_id && $personality_type->id == $row->personality_type_id)
                        {
                            $true_positive++;
                        }

                        if($personality_type->id != $game_character->personality_type_id && $personality_type->id == $row->personality_type_id)
                        {
                            $false_positive++;
                        }

                        if($personality_type->id != $game_character->personality_type_id && $personality_type->id != $row->personality_type_id)
                        {
                            $true_negative++;
                        }

                        if($personality_type->id == $game_character->personality_type_id && $game_character->personality_type_id != $row->personality_type_id)
                        {
                            $false_negative++;
                        }
                    }
                }
                $specific_profiles[$personality_type->short_title]   = ['true_positive' => $true_positive, 'false_positive' => $false_positive, 'true_negative' => $true_negative, 'false_negative' => $false_negative, 'correct_engagements' => 0, 'total_engagements' => 0];
            }

            $game_characters   = GameCharacter::where(['game_play_id' => $game_play->id])->get();

            foreach($game_characters as $game_character)
            {
                $short_title = PersonalityType::find($game_character->personality_type_id)->short_title;
                $specific_profiles[$short_title]['correct_engagements'] += $game_character->leader->gameStagewiseEngagements()->whereIn('game_stage_id', $stages->all())->where(['accuracy' => 'correct'])->count();
                $specific_profiles[$short_title]['total_engagements'] += 28;
            }

            //All The Calculations
            $per_descisions_made        = round($decisions_made_after_observing / ($total_decisions_made == 0 ? 1 : $total_decisions_made) * 100);
            $per_potential_diagnosis    = round($no_of_potential_diagnose / 35 * 100);
            $per_potential_engagements  = round($no_of_potential_engagements / 140 * 100);
            $per_approach_quality       = round(($per_descisions_made+$per_potential_diagnosis+$per_potential_engagements) / 3);

            $per_understanding_others   = 0;
            $per_adapting_to_others     = 0;
            foreach($leaders as $leader)
            {
                $diagnose   = $leader->gameCharacter()->where('game_play_id', $game_play->id)->first()->personalityType;
                $correct_diagnose   = 0;
                $diagnose_count     = 0;
                $correct_engagements_count  = 0;
                $diagnose_count     = 0;
                foreach($game_data->where('leader_id', $leader->id)->all() as $data)
                {
                    if(isset($data->personalityType->short_title))
                    {
                        if($data->personalityType->short_title == $diagnose->short_title)
                        {
                            $correct_diagnose++;
                        }
                    }
                    $diagnose_count++;
                }
                $per_understanding_others += round($correct_diagnose / 7 * 100);
                foreach($stages as $stage)
                {
                    $correct_engagements    = $leader->gameStagewiseEngagements()->where(['game_stage_id' => $stage, 'leader_id' => $leader->id, 'accuracy' => 'correct'])->count();
                    $correct_engagements_count += $correct_engagements;
                }
                $per_adapting_to_others += round($correct_engagements_count / 28 * 100);
            }
            $per_understanding_others = round($per_understanding_others / $leaders->count());
            $per_adapting_to_others   = round($per_adapting_to_others / $game_characters->count());

            $user->score       = round(($per_understanding_others + $per_adapting_to_others) / 2, 2);
        }

        return view('reports.leaderboard')->with(['users' => $users->sortByDesc('score')->take(10)]);
    }
}
