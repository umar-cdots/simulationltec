<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\GamePlay;
use App\GameCharacter;
use App\GameStage;
use App\GameStagewiseData;
use App\GameStagewiseEngagement;
use App\Leader;
use App\Tenure;
use App\Engagement;
use App\EngagementObservation;
use App\Observation;
use App\PersonalityType;

class AjaxController extends Controller
{
	private $Response;

	public function __construct()
	{
		$this->Response = ['status' => "", 'message' => "", 'data' => array()];
	}

    public function observation(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'leader_id' 	=> 'required|exists:leaders,id',
            'game_play_id' 	=> 'required|exists:game_plays,id',
            'game_stage_id'	=> 'required|exists:game_stages,id',
            'stage_no'		=> 'sometimes'
        ],
    	[
    		'leader_id.*' 	=> "Something Went Wrong",
    		'game_play_id.*'=> "Press the Play Button First"
    	]);

    	if(!$validator->fails())
    	{
    		$leader = Leader::find($request->leader_id);

    		if($request->stage_no > 1)
    		{
    			$stagewise_data = GameStagewiseData::where(['game_stage_id' => $request->game_stage_id, 'leader_id' => $request->leader_id])->first();

    			$observation_text 	= $leader->name . ' ' . $stagewise_data->observation->observation;
    		}
    		else
    		{
    			$observation_text 	= $leader->name . ' ' . $leader->observation;
    		}

    		$this->Response['status'] 	= "success";
	    	$this->Response['message'] 	= "";
	    	$this->Response['data'] 	= ['observation' => $observation_text];
    	}
    	else
    	{
    		$this->Response['status'] 	= "error";
	    	$this->Response['message'] 	= $validator->errors()->first('game_play_id');
	    	$this->Response['data'] 	= ['validator' => $validator->errors()];
    	}

    	return $this->Response;
    }

    public function startGame(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'project_id' 	=> 'required|exists:projects,id'
        ],
    	[
    		'project_id.*' 	=> "Something Went Wrong"
    	]);

    	if(!$validator->fails())
    	{
	    	$game 				= new GamePlay;
	    	$game->user_id 		= Auth::user()->id;
	    	$game->game_starts 	= date('Y-m-d H:i:s');
	    	$game->save();

	    	$game_stage 				= new GameStage;
	    	$game_stage->game_play_id 	= $game->id;
	    	$game_stage->stage_no 		= 1;
	    	$game_stage->save();

            $personality_types  = PersonalityType::all()->pluck('id');
            $assigned_types     = collect();

	    	$leaders 			= Leader::where('project_id', $request->project_id)->get();
	    	foreach($leaders as $leader)
	    	{
	    		$game_character = new GameCharacter;
	    		$game_character->game_play_id 	= $game->id;
                $game_character->leader_id      = $leader->id;
	    		$game_character->personality_type_id  	= $personality_types->diff($assigned_types)->shuffle()->first();
                if(empty($game_character->personality_type_id))
                {
                    $game_character->personality_type_id = $personality_types->shuffle()->first();
                }
	    		$game_character->save();

		    	$stagewise_data 					 = new GameStagewiseData;
		    	$stagewise_data->game_stage_id 	     = $game_stage->id;
		    	$stagewise_data->leader_id 		     = $leader->id;
		    	$stagewise_data->personality_type_id = 0;
                $stagewise_data->observation_id      = 0;
                $stagewise_data->leadership_effectiveness = $leader->initial_leadership_effectiveness;
                $stagewise_data->initial_value       = 0;
		    	$stagewise_data->productivity        = 0;
		    	$stagewise_data->save();

                $assigned_types->push($game_character->personality_type_id);
	    	}

	    	$this->Response['status'] 	= "success";
	    	$this->Response['message'] 	= "Simulation Started";
	    	$this->Response['data'] 	= ['game_play_id' => $game->id, 'game_stage_id' => $game_stage->id, 'stage_no' => $game_stage->stage_no];
	    }
	    else
	    {
	    	$this->Response['status'] 	= "error";
	    	$this->Response['message'] 	= $validator->errors()->first('project_id');
	    	$this->Response['data'] 	= ['validator' => $validator->errors()];
	    }

    	return $this->Response;
    }

    public function endGame(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'project_id'    => 'required|exists:projects,id',
            'game_play_id'  => 'required|exists:game_plays,id',
            'type'          => 'required|in:complete,timesup'
        ],
        [
            'project_id.*'      => "Something Went Wrong.",
            'game_play_id.*'    => "You need to Play Simulation First."
        ]);

        if(!$validator->fails())
        {
            $game               = GamePlay::where(['id' => $request->game_play_id, 'user_id' => Auth::user()->id])->firstOrFail();

            $game->game_ends    = date('Y-m-d H:i:s');
            $game->save();

            if($request->type == "complete")
            {
                $game->gameStages()->orderBy('id', 'DESC')->first()->forceDelete();
            }

            $this->Response['status']   = "success";
            $this->Response['message']  = "Simulation Ends.";
            $this->Response['data']     = [];
        }
        else
        {
            $this->Response['status']   = "error";
            $this->Response['message']  = $validator->errors()->first('game_play_id');
            $this->Response['data']     = ['validator' => $validator->errors()];
        }

        return $this->Response;
    }


    public function nextStage(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'project_id'			=> 'required|bail|exists:projects,id',
    		'stage_no'				=> 'required|min:1|max:'. Tenure::where('project_id', $request->project_id)->first()->total_stages,
            'game_play_id' 			=> 'required|exists:game_plays,id',
            'game_stage_id'			=> 'required|exists:game_stages,id',
            'engagement'            => 'array|bail',
            'engagement.*.*' 		=> 'required|exists:engagements,id',
            'personality_type'      => 'array|bail',
            'personality_type.*' 	=> ['required', function($attribute, $value, $fail){
            	if($value == -1)
            		return true;
            	return PersonalityType::find($value)->count();
            }],
        ],
    	[
    		'project_id.*'			=> "Something Went Wrong.",
    		'game_play_id.*'		=> "You need to Play Simulation First.",
    		'game_stage_id.*'		=> "You need to Play Simulation First.",
    		'stage_no.*'			=> "Simulaton Ends.",
    		'engagement.*' 			=> "Engagement Required.",
    		'personality_type.*' 	=> "Interpretation Required."
    	]);

    	if(!$validator->fails())
    	{	
            if(GameStage::where(['game_play_id' => $request->game_play_id, 'stage_no' => $request->stage_no + 1])->count() >= 1)
                return abort(422);

    		$possible_observations		= collect();
    		$assigned_observations 		= collect();

            $game_play                  = GamePlay::find($request->game_play_id);
            if(empty($game_play->game_ends))
            {
                $game_stages_count          = $game_play->gameStages->count();

                $old_game_stage             = GameStage::find($request->game_stage_id);

                $game_stage                 = new GameStage;
                $game_stage->game_play_id   = $request->game_play_id;
                $game_stage->stage_no       = $old_game_stage->stage_no + 1;
                $game_stage->save();

                $progress                   = array();

                $previous_initial_value     = 1;
                $previous_productivity      = 0;
                $previous_leader_id         = 0;

                $i_count                    = 0;

        		foreach($request->personality_type as $leader_id => $personality_type_id)
        		{
                    $i_count++;

    	    		$game_stagewise_data                         = GameStagewiseData::where(['game_stage_id' => $request->game_stage_id, 'leader_id' => $leader_id])->first();
    	    		$game_stagewise_data->personality_type_id    = $personality_type_id == -1 ? 0 : $personality_type_id;
    	    		$game_stagewise_data->save();

                    $game_character         = GameCharacter::where(['game_play_id' => $request->game_play_id, 'leader_id' => $leader_id])->first();

                    $leader                 = Leader::find($leader_id);
                    //Little Hack
                    $leader->personality_type_id = $game_character->personality_type_id;

                    $total_points_earned    = 0;

        			$engagements 	        = $request->engagement[$leader_id];
                    $personality_type       = PersonalityType::find($leader->personality_type_id); 
        			foreach($engagements as $engagement_id)
        			{
        				$engagement 	    = Engagement::find($engagement_id);  

                        if($engagement->personality_type_id == $leader->personality_type_id)
                        {
                            $accuracy               = "correct";
                            $points_earned          = $personality_type->points_per_engagment;
                            $total_points_earned    += $points_earned;
                        }
                        elseif(PersonalityType::find($engagement->personality_type_id)->oposite_personality_short_title == $personality_type->short_title)
                        {
                            $accuracy                = "opposite";
                            $points_earned           = $personality_type->points_per_engagment * -1;
                            $total_points_earned    += $points_earned;
                        }
                        else
                        {
                            $accuracy                = "wrong";
                            $points_earned           = 0;
                            $total_points_earned    += $points_earned;       
                        }

        				$game_stepwise_engagement 				= new GameStagewiseEngagement;
                        $game_stepwise_engagement->project_id   = $request->project_id;
        				$game_stepwise_engagement->game_play_id = $request->game_play_id;
        				$game_stepwise_engagement->game_stage_id= $request->game_stage_id;
        				$game_stepwise_engagement->leader_id 	= $leader_id;
                        $game_stepwise_engagement->engagment_id = $engagement->id;
                        $game_stepwise_engagement->accuracy     = $accuracy;
        				$game_stepwise_engagement->points_earned= $points_earned;
        				$game_stepwise_engagement->save();

                        $possible_observations = $possible_observations->merge(EngagementObservation::where(['engagement_id' => $engagement->id, 'personality_type_id' => $leader->personality_type_id])->get()->pluck('observation_id'));
                        $old_assigned_observations = GameStagewiseData::where('leader_id', $leader_id)->whereIn('game_stage_id', GameStage::where('game_play_id', $request->game_play_id)->get()->pluck('id'))->get()->pluck('observation_id');
                        $temp_remaings = $possible_observations->diff($old_assigned_observations);
                        if($temp_remaings->count() > 0)
                        {
                            $possible_observations = $temp_remaings;
                        }
                        else
                        {
                            $possible_observations = Observation::limit(4)->orderBy('id', 'DESC')->get()->pluck('id')->diff($old_assigned_observations);
                        }

        				// if($personality_type_id == -1)
            //             {
            //                 $possible_observations = $possible_observations->merge(Observation::limit(4)->orderBy('id', 'DESC')->get()->pluck('id'));
            //                 $old_assigned_observations = GameStagewiseData::where('leader_id', $leader_id)->whereIn('game_stage_id', GameStage::where('game_play_id', $request->game_play_id)->get()->pluck('id'))->get()->pluck('observation_id');
            //                 $temp_remaings = $possible_observations->diff($old_assigned_observations);
            //                 if($temp_remaings->count() > 0)
            //                 {
            //                     $possible_observations = $temp_remaings;
            //                 }
            //                 else
            //                 {
            //                     $possible_observations = Observation::limit(4)->orderBy('id', 'DESC')->get()->pluck('id')->diff($old_assigned_observations);
            //                 }
            //             }
        				// else
        				// {
            //                 $possible_observations = $possible_observations->merge(EngagementObservation::where(['engagement_id' => $engagement->id, 'personality_type_id' => $personality_type_id])->get()->pluck('observation_id'));
            //                 $old_assigned_observations = GameStagewiseData::where('leader_id', $leader_id)->whereIn('game_stage_id', GameStage::where('game_play_id', $request->game_play_id)->get()->pluck('id'))->get()->pluck('observation_id');
            //                 $temp_remaings = $possible_observations->diff($old_assigned_observations);
            //                 if($temp_remaings->count() > 0)
            //                 {
            //                     $possible_observations = $temp_remaings;
            //                 }
            //                 else
            //                 {
            //                     $possible_observations = Observation::limit(4)->orderBy('id', 'DESC')->get()->pluck('id')->diff($old_assigned_observations);
            //                 }
        				// }
        			}

                    $leadership_effectiveness = $game_stagewise_data->leadership_effectiveness + ($total_points_earned / 100);
                    // $initial_value            = $game_stagewise_data->leadership_effectiveness * $leader->step->quarterly_target;
                    $initial_value            = $leadership_effectiveness * $leader->step->quarterly_target;

                    if($game_stages_count == 1 && $i_count != count($request->personality_type))
                    {
                        $productivity             = $leader->step->initial_productivity + ($previous_initial_value * $initial_value);
                    }
                    elseif($i_count == count($request->personality_type))
                    {
                        $previous_step_all_productivity = GameStagewiseData::whereIn('game_stage_id', GameStage::where('game_play_id', $request->game_play_id)->get()->pluck('id')->all())->where(['leader_id' => $previous_leader_id])->get()->pluck('productivity')->sum();
                        $productivity             = $previous_step_all_productivity * $initial_value;
                    }
                    else
                    {
                        $productivity             = $previous_initial_value * $initial_value;
                    }

                    //Previous GameStagewiseData
                    // $game_stagewise_data->initial_value         = $initial_value;
                    // $game_stagewise_data->productivity          = $productivity;
                    // $game_stagewise_data->save();

        			$game_stagewise_data 					    = new GameStagewiseData;
        			$game_stagewise_data->game_stage_id 		= $game_stage->id;
        			$game_stagewise_data->leader_id 			= $leader_id;
        			$game_stagewise_data->personality_type_id   = 0;
        			$game_stagewise_data->observation_id		= $this->randomObservationSelection($possible_observations, $assigned_observations);
                    $game_stagewise_data->leadership_effectiveness  = number_format($leadership_effectiveness, 5, '.', '');
                    $game_stagewise_data->initial_value         = number_format($initial_value, 5, '.', '');
                    $game_stagewise_data->productivity          = number_format($productivity, 5, '.', '');
        			$game_stagewise_data->save();

                    // $stepwise_target_achived    = new StepwiseTargetAchived;
                    // $stepwise_target_achived->game_play_id      = $request->game_play_id;
                    // $stepwise_target_achived->game_stage_id     = $request->game_stage_id;
                    // $stepwise_target_achived->leader_id         = $leader->id;
                    // $stepwise_target_achived->step_id           = $leader->step_id;
                    // $stepwise_target_achived->initial_value     = $initial_value;
                    // $stepwise_target_achived->quarterly_productivity  = $productivity;
                    // $stepwise_target_achived->save();

                    $previous_initial_value = $initial_value;
                    $previous_productivity  = $productivity;
                    $previous_leader_id     = $leader_id;
                    $assigned_observations->push($game_stagewise_data->observation_id);
        			$possible_observations	= collect();

                    $progress[]             =   ['leader_id' => $leader_id, 'initial_value' => $initial_value, 'productivity' => $productivity];
                }
    		}
            else
            {
                $this->Response['status']   = "error";
                $this->Response['message']  = "Simulation Already Ends.";
                $this->Response['data']     = ['game_play' => $game_play];
            }

    		$this->Response['status'] 	= "success";
	    	$this->Response['message'] 	= "Stage # {$request->stage_no} Completed Successfully.";
	    	$this->Response['data']		= ['game_stage_id' => $game_stage->id, 'progress' => $progress];
		}
	    else
	    {
	    	$this->Response['status'] 	= "error";
	    	$this->Response['message'] 	= $validator->errors()->first('project_id');
	    	$this->Response['data'] 	= ['validator' => $validator->errors()];
	    }

    	return $this->Response;
    }

    public function observed(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'game_play_id'          => 'required|exists:game_plays,id',
            'game_stage_id'         => 'required|exists:game_stages,id',
            'leader_id'             => 'required|exists:leaders,id'
        ],
        [
            'game_play_id.*'        => "You need to Play Simulation First.",
            'game_stage_id.*'       => "You need to Play Simulation First.",
            'leader_id.*'           => "Something Went Wrong."
        ]);

        if(!$validator->fails())
        {
            $game_stagewise_data = GameStagewiseData::where(['game_stage_id' => $request->game_stage_id, 'leader_id' => $request->leader_id])->firstOrFail();

            $game_stagewise_data->is_observed   = true;
            $game_stagewise_data->save();

            $this->Response['status']           = "success";
        }
        else
        {
            $this->Response['status']   = "error";
            $this->Response['message']  = $validator->errors()->first('game_play_id');
            $this->Response['data']     = ['validator' => $validator->errors()];
        }

        return $this->Response;
    }

    public function engagedAfterObserving(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'game_play_id'          => 'required|exists:game_plays,id',
            'game_stage_id'         => 'required|exists:game_stages,id',
            'leader_id'             => 'required|exists:leaders,id'
        ],
        [
            'game_play_id.*'        => "You need to Play Simulation First.",
            'game_stage_id.*'       => "You need to Play Simulation First.",
            'leader_id.*'           => "Something Went Wrong."
        ]);

        if(!$validator->fails())
        {
            $game_stagewise_data = GameStagewiseData::where(['game_stage_id' => $request->game_stage_id, 'leader_id' => $request->leader_id])->firstOrFail();

            $game_stagewise_data->engaged_after_observing   = true;
            $game_stagewise_data->save();

            $this->Response['status']           = "success";
        }
        else
        {
            $this->Response['status']   = "error";
            $this->Response['message']  = $validator->errors()->first('game_play_id');
            $this->Response['data']     = ['validator' => $validator->errors()];
        }

        return $this->Response;
    }

    private function randomObservationSelection($observations, $assigned_observations)
    {
    	$observation_id = 0;
    	$diff 			= $observations->diff($assigned_observations->toArray());
    	
    	if($diff->count() > 0)
    	{//echo "1: ".$diff->random().print_r($diff, true);die;
    		return $diff->random();
    	}
    	else
    	{
    		if($observations->count() > 0)
    		{//echo "2: ".$observations->random();die;
    			return $observations->random();
    		}
    		else
    		{//echo "3: ".Observation::limit(4)->orderBy('id', 'DESC')->get()->pluck('id')->random();die;
    			return Observation::limit(4)->orderBy('id', 'DESC')->get()->pluck('id')->random();
    		}
    	}
    }
}
