<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Tenure;
use App\Steps;
use App\EngagementHeading;
use App\PersonalityType;

class SimulationController extends Controller
{
    public function index()
    {
    	$project 	= Project::where('title', "LTEC")->first();
    	$tenure 	= Tenure::where('project_id', $project->id)->first();
    	$steps 		= Steps::where('project_id', $project->id)->get();
    	$engagementHeadings = EngagementHeading::where('project_id', $project->id)/*->where('quarter_no', 1)*/->get();
    	$personalityTypes	= PersonalityType::all();

    	return view('simulation.index')->with(['project' => $project, 'tenure' => $tenure, 'steps' => $steps, 'engagementHeadings' => $engagementHeadings, 'personalityTypes' => $personalityTypes]);
    }
}
