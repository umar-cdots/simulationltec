<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Engagement extends Model
{
    public function engagementHeading()
    {
    	return $this->belongsTo(EngagementHeading::class);
    }
}
