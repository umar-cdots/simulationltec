<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EngagementObservation extends Model
{
    public function observation()
    {
    	return $this->belongsTo(Observation::class);
    }
}
