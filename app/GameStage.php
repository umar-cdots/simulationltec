<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameStage extends Model
{
    public function game()
    {
    	return $this->belongsTo(GamePlay::class);
    }

    public function gameStagewiseData()
    {
    	return $this->hasMany(GameStagewiseData::class);
    }
}
