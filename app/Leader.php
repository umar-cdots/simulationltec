<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leader extends Model
{
    public function step()
    {
    	return $this->belongsTo(Steps::class);
    }

    public function gameCharacter()
    {
    	return $this->hasOne(GameCharacter::class);
    }

    public function gameStagewiseEngagements()
    {
    	return $this->hasMany(GameStagewiseEngagement::class, 'leader_id');
    }
}
