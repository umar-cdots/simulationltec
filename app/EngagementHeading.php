<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EngagementHeading extends Model
{
    public function engagements()
    {
    	return $this->hasMany(Engagement::class);
    }
}
