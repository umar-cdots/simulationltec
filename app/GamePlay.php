<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GamePlay extends Model
{
    public function gameStages()
    {
    	return $this->hasMany(GameStage::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function gameCharacter()
    {
    	return $this->hasMany(GameCharacter::class);
    }
}
