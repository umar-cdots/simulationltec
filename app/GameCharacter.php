<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameCharacter extends Model
{
    public function gamePlay()
    {
    	return $this->belongsTo(GamePlay::class);
    }

    public function leader()
    {
    	return $this->belongsTo(Leader::class);
    }

    public function personalityType()
    {
    	return $this->belongsTo(PersonalityType::class);
    }
}
