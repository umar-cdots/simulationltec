<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
    public function project()
    {
    	return $this->belongsTo(Project::class);
    }

    public function users()
    {
    	return $this->hasMany(User::class, 'session_id');
    }
}
