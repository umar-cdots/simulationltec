<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameStagewiseData extends Model
{
    public function observation()
    {
    	return $this->belongsTo(Observation::class);
    }

    public function personalityType()
    {
    	return $this->belongsTo(PersonalityType::class);
    } 
}
