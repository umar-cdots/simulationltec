<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Steps extends Model
{
    public function leader()
    {
    	return $this->hasOne(Leader::class, 'step_id');
    }
}
