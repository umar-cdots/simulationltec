<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	public function sessions()
	{
		return $this->hasMany(Sessions::class);
	}
}
