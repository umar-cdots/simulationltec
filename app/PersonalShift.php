<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalShift extends Model
{
    public function personalityType()
    {
    	return $this->belongsTo(PersonalityType::class);
    }
}
