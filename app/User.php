<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function session()
    {
        return $this->belongsTo(Sessions::class, 'session_id');
    }

    public function gamePlays()
    {
        return $this->hasMany(GamePlay::class);
    }

    /**
     * Scope a query to only include particular session users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSession($query, $session_id = '')
    {
        return empty($session_id) ? $query : $query->where('session_id', $session_id);
    }
}
