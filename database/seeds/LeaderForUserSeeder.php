<?php

use Illuminate\Database\Seeder;
use App\Sessions;
use App\Leader;
use App\User;
use App\LeaderForUser;

class LeaderForUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = User::where('user_type', 'user')->first();

        $instance = new LeaderForUser;
        $instance->session_id 	= Sessions::first()->id;
        $instance->leader_id	= Leader::where('name', "Dimitry Avech")->first()->id;
        $instance->user_id 		= $user->id;
        $instance->save();

        $instance = new LeaderForUser;
        $instance->session_id 	= Sessions::first()->id;
        $instance->leader_id	= Leader::where('name', "Agnes Savyr")->first()->id;
        $instance->user_id 		= $user->id;
        $instance->save();
        
        $instance = new LeaderForUser;
        $instance->session_id 	= Sessions::first()->id;
        $instance->leader_id	= Leader::where('name', "Abhinav Sharma")->first()->id;
        $instance->user_id 		= $user->id;
        $instance->save();
        
        $instance = new LeaderForUser;
        $instance->session_id 	= Sessions::first()->id;
        $instance->leader_id	= Leader::where('name', "Diana Thompson")->first()->id;
        $instance->user_id 		= $user->id;
        $instance->save();
        
        $instance = new LeaderForUser;
        $instance->session_id 	= Sessions::first()->id;
        $instance->leader_id	= Leader::where('name', "Anita Lopez")->first()->id;
        $instance->user_id 		= $user->id;
        $instance->save();
    }
}
