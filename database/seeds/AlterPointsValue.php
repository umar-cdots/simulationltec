<?php

use Illuminate\Database\Seeder;
use App\PersonalityType;

class AlterPointsValue extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PersonalityType::where('id', '>=', 1)->update(['points_per_engagment' => 2.8]);
    }
}
