<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(ProjectSeeder::class);
        // $this->call(UserSeeder::class);
        // $this->call(PersonalityTypesSeeder::class);
        // $this->call(StepsSeeder::class);
        // $this->call(LeaderSeeder::class);
        // $this->call(TenureSeeder::class);
        // $this->call(EngagementSeeder::class);
        $this->call(NewEngagmentsSeeder::class);
        $this->call(ObservationSeeder::class);
        // $this->call(StepwiseTargetsSeeder::class);
        // $this->call(PersonalShiftsSeeder::class);
        $this->call(AlterPointsValue::class);
    }
}
