<?php

use Illuminate\Database\Seeder;
use App\Sessions;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user 				= new User;
        $user->name 		= "BCG";
        $user->nick_name 	= "bcg";
        $user->email 		= "admin@bcg.com";
        $user->password 	= bcrypt('123456');
        $user->gender 		= "male";
        $user->user_type 	= "super_admin";
        $user->save();
    }
}
