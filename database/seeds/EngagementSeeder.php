<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\PersonalityType;
use App\EngagementHeading;
use App\Engagement;

class EngagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$project	= Project::where('title', "LTEC")->first();

    	/*1st*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->title 		= "Assign a project";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Short-term and challenging";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "With heavy stakeholder management";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Collaborative, inter functional";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Requiring high quality and accuracy";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();
    	
    	/*2nd*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->title 		= "Assign a role";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "In a strategic, powerful position";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "As the spokesperson of the team";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "That listens to people concerns";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "That assesses quality of the outputs";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();
    	
    	/*3rd*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->title 		= "Require an action";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Ensure closure in a decisive meeting";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Host a networking event";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Support a colleague in trouble";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Improve efficiency of team processes";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();
    	
    	/*4th*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->title 		= "Support in decision making";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Make a difficult decision";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Persuade others towards a specific decision";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Coordinate the team to align a decision";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Verify that a decision is sound";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();
    }
}
