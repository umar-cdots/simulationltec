<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\Steps;
use App\Leader;
use App\PersonalityType;

class LeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project    = Project::where('title', "LTEC")->first();

        $leader 			= new Leader;
        $leader->project_id	= $project->id;
        $leader->name 		= "Dimitry Avech";
        $leader->observation    = "is a well respected leader in the company. During your interviewes with employees that worked for him, stood out his deep expertise and tenacity";
        $leader->designation 						= "SVP";
        $leader->initial_leadership_effectiveness 	= 0.87;
        $leader->logo 		= "vendor/adminlte/dist/img/avatar5.png";
        $leader->step_id    = Steps::where('title', "Access")->first()->id;
        $leader->personality_type_id    = PersonalityType::where('short_title', "C")->first()->id;
        $leader->save();

        $leader 			= new Leader;
        $leader->project_id	= $project->id;
        $leader->name 		= "Agnes Savyr";
        $leader->observation    = "has a volatile track record of performance. During your first conversation with her, she complained about the performance of her direct reports";
        $leader->designation 						= "SVP";
        $leader->initial_leadership_effectiveness 	= 0.87;
        $leader->logo 		= "vendor/adminlte/dist/img/avatar3.png";
        $leader->step_id    = Steps::where('title', "Planning")->first()->id;
        $leader->personality_type_id    = PersonalityType::where('short_title', "S")->first()->id;
        $leader->save();

        $leader 			= new Leader;
        $leader->project_id	= $project->id;
        $leader->name 		= "Abhinav Sharma";
        $leader->observation    = "during the first interview, was open to share his perspective on leadership challenges and pain points";
        $leader->designation 						= "SVP";
        $leader->initial_leadership_effectiveness 	= 0.87;
        $leader->logo 		= "vendor/adminlte/dist/img/avatar04.png";
        $leader->step_id    = Steps::where('title', "Contract Approval")->first()->id;
        $leader->personality_type_id    = PersonalityType::where('short_title', "C")->first()->id;
        $leader->save();

        $leader 			= new Leader;
        $leader->project_id	= $project->id;
        $leader->name 		= "Diana Thompson";
        $leader->observation    = "is a relatively young leader. During the first conversation, she mentioned her passion for the industry";
        $leader->designation 						= "SVP";
        $leader->initial_leadership_effectiveness 	= 0.87;
        $leader->logo 		= "vendor/adminlte/dist/img/avatar2.png";
        $leader->step_id    = Steps::where('title', "Installation")->first()->id;
        $leader->personality_type_id    = PersonalityType::where('short_title', "I")->first()->id;
        $leader->save();

        $leader 			= new Leader;
        $leader->project_id	= $project->id;
        $leader->name 		= "Anita Lopez";
        $leader->observation    = "is a tenured leader in the company. You didn't manage to have a conversation with her yet, and her direct reports didn't mention any obvious strength or development area";
        $leader->designation 						= "SVP";
        $leader->initial_leadership_effectiveness 	= 0.87;
        $leader->logo 		= "vendor/adminlte/dist/img/avatar6.png";
        $leader->step_id    = Steps::where('title', "Operation")->first()->id;
        $leader->personality_type_id    = PersonalityType::where('short_title', "D")->first()->id;
        $leader->save();
    }
}
