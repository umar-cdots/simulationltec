<?php

use Illuminate\Database\Seeder;
use App\PersonalityType;
use App\PersonalShift;

class PersonalShiftsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv_path = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'personalShifts.csv';

    	if(is_file($csv_path))
    	{
    		$row 		= 1;
			if (($handle = fopen($csv_path, "r")) !== FALSE) 
			{
		  		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			  	{
				    $no_of_columns = count($data);
				    $this->command->info("$no_of_columns Columns in #{$row} Row.");
				    if($no_of_columns < 3)
				    {
				    	$this->command->error("Invalid no fo Columns.");
				    	continue;
				    }
				    $row++;
				    $personality_type 	= PersonalityType::where('short_title', trim($data[0]))->first();
				    $personal_shift  	= new PersonalShift;
				    $personal_shift->personality_type_id = $personality_type->id;
				    $personal_shift->name 				 = $data[1];
				    $personal_shift->description 		 = $data[2];
				    $personal_shift->save();
				    $this->command->info("Row #{$row} Saved.");
			  	}
			  	fclose($handle);
			}
    	}
    	else
    	{
    		$this->command->error("Observations File Doesn't Exists.");
    	}
    }
}
