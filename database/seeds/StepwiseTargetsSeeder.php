<?php

use Illuminate\Database\Seeder;
use App\StepwiseTarget;
use App\Steps;
use App\Project;

class StepwiseTargetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$project 						= Project::where('title', 'LTEC')->first();

    	foreach(['Access' => [14.37,14.96,15.56,16.18,16.81,17.45,18.11,18.78], 'Planning' => [9.78,10.13,10.49,10.87,11.27,11.68,12.11,12.56], 'Contract Approval' => [6.87,7.21,7.58,7.96,8.35,8.77,9.20,9.65], 'Installation' => [4.42,4.77,5.13,5.51,5.90,6.32,6.75,7.20], 'Operation' => [615.40,679.56,748.44,822.28,901.30,985.74,1075.84,1171.83]] as $step_title => $target_values)
    	{
	    	$step 							= Steps::where(['title' => $step_title, 'project_id' => $project->id])->first();
	    	$stage_no 						= 1;

	    	foreach($target_values as $target_value)
	    	{
	    		$stepwise_target 				= new StepwiseTarget;
		        $stepwise_target->step_id 		= $step->id;
		        $stepwise_target->stage_no 		= $stage_no++;
		        $stepwise_target->target_value 	= $target_value;
		        $stepwise_target->save();
	    	}
	    }
    }
}
