<?php

use Illuminate\Database\Seeder;
use App\PersonalityType;

class PersonalityTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personality = new PersonalityType;
        $personality->short_title 	= "D";
        $personality->title 		= "Dominant";
        $personality->description 	= "Outgoing and task oriented.";
        $personality->oposite_personality_short_title 	= "S";
        $personality->points_per_engagment 				= 0.7;
        $personality->save();

        $personality = new PersonalityType;
        $personality->short_title 	= "I";
        $personality->title 		= "Inspiring";
        $personality->description 	= "Outgoing and people oriented.";
        $personality->oposite_personality_short_title 	= "C";
        $personality->points_per_engagment 				= 0.7;
        $personality->save();
        
        $personality = new PersonalityType;
        $personality->short_title 	= "S";
        $personality->title 		= "Supportive";
        $personality->description 	= "Reserved and people oriented.";
        $personality->oposite_personality_short_title 	= "D";
        $personality->points_per_engagment 				= 0.7;
        $personality->save();
        
        $personality = new PersonalityType;
        $personality->short_title 	= "C";
        $personality->title 		= "Cautious";
        $personality->description 	= "Reserved and task oriented.";
        $personality->oposite_personality_short_title 	= "I";
        $personality->points_per_engagment 				= 0.7;
        $personality->save();
    }
}
