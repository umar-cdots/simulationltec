<?php

use Illuminate\Database\Seeder;
use App\Engagement;
use App\EngagementObservation;
use App\Observation;
use App\PersonalityType;

class ObservationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Observation::truncate();
    	EngagementObservation::truncate();
    	
    	$csv_path = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'observations.csv';

    	if(is_file($csv_path))
    	{
    		$row 		= 1;
    		$p_index 	= 0;
    		$count 		= 0;
    		$p_types 	= ['D', 'I', 'S', 'C'];
			if (($handle = fopen($csv_path, "r")) !== FALSE) 
			{
		  		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			  	{
				    $no_of_columns = count($data);
				    $this->command->info("$no_of_columns Columns in #{$row} Row.");
				    $row++;
				    $engagement 	= null;
				    $observation 	= null;
				    $eo_instance 	= null;
				    for ($c = 0; $c < $no_of_columns; $c++) 
				    {
				    	if($c == 0)
				    	{
				    		$this->command->info($data[$c]);
				    		if($data[0] == 'No behaviors')
					    	{
					    		continue;
					    	}
					    	else
					    	{
					    		$engagement = Engagement::where('title', $data[$c])->first();
					    		if(empty($engagement))
					    		{
					    			$this->command->error("{$data[$c]} Doesn't Found in Engagement.");
					    			break;
					    		}
					    	}
				    	}
				    	else
				    	{
				    		if(empty($data[$c]))
				    			continue;
				    		$this->command->info($row . ' => ' . $c);
				    		$observation = new Observation;
				    		$observation->observation = trim($data[$c]);
				    		$observation->save();
				    		if(!empty($engagement))
				    		{
					    		$eo_instance = new EngagementObservation;
					    		$eo_instance->engagement_id 		= $engagement->id;
					    		$eo_instance->personality_type_id	= PersonalityType::where('short_title', $p_types[$p_index])->first()->id;
					    		$eo_instance->observation_id 		= $observation->id;
					    		$eo_instance->save();
					    	}
				    	}
				    	$count++;
				    	if($count % 4 == 0)
				    	{
				    		$p_index++;
				    		if(!isset($p_types[$p_index]))
				    		{
				    			$p_index = 0;
				    		}
				    	}
				    }
		    		$p_index 	= 0;
		    		$count 		= 0;
			  	}
			  	fclose($handle);
			}
    	}
    	else
    	{
    		$this->command->error("Observations File Doesn't Exists.");
    	}
    }
}
