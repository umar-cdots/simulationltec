<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\PersonalityType;
use App\EngagementHeading;
use App\Engagement;

class NewEngagmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	EngagementHeading::truncate();
    	Engagement::truncate();

        $project	= Project::where('title', "LTEC")->first();

        /*1st*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->quarter_no 	= 1;
        $engagment_heading->title 		= "Define the communication style";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Communicating mainly about decisions, getting straight to the point";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Asking questions to give the leader space to talk and shine in meetings";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Spending some time at the beginning of each meeting to do small talk";         
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Communicating thoroughly about data, analysis and implications";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();

        /*2nd*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->quarter_no 	= 2;
        $engagment_heading->title 		= "Help the leader to focus on projects";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "That are short-term and challenging";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "With heavy stakeholder management";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "That are collaborative and interfunctional";         
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Requiring high quality and accuracy";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();

        /*3rd*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->quarter_no 	= 3;
        $engagment_heading->title 		= "Suggest working model";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "That defines individual accountability and consequences";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "That defines the team vision and expectations";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "With shared responsibilities to be achieved collectively";         
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "With rules and processes for the team";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();

        /*4th*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->quarter_no 	= 4;
        $engagment_heading->title 		= "Influence decision making";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Suggesting the leader to make a difficult decision";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Suggesting the leader to persuade others towards a specific decision";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Suggesting the leader to coordinate the team to align a decision";         
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Suggesting the leader to verify the soundness of decisions";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();

        /*5th*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->quarter_no 	= 5;
        $engagment_heading->title 		= "Help the team with an action";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Ensure closure in a decisive meeting";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Host a networking event";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Support a client team member in trouble";         
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Improve efficiency of team processes";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();

        /*6th*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->quarter_no 	= 6;
        $engagment_heading->title 		= "Support managing a crisis";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Ask the leader to make fast top down decisions";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Ask the leader to influence stakeholders to buy time";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Ask the leader to help colleagues in need";         
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Ask the leader to analyze options to contain the crisis";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();

        /*7th*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->quarter_no 	= 7;
        $engagment_heading->title 		= "Accelerateprogress";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "By helping the team to deliver results faster";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "By helping the team to interpret the leader's vision";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "By coordinating with the team to deliver in a joint effort";         
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "By improving the team's working model efficiency";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();

        /*8th*/
        $engagment_heading = new EngagementHeading;
        $engagment_heading->project_id 	= $project->id;
        $engagment_heading->quarter_no 	= 8;
        $engagment_heading->title 		= "Present year end results";
        $engagment_heading->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "With sharp numbers about the project results";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "D")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "With an inspiring pitch on video";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "I")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "Letting the team members to present the outputs";         
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "S")->first()->id;
        $engagment->save();

        $engagment 	= new Engagement;
        $engagment->project_id 				= $project->id;
        $engagment->engagement_heading_id 	= $engagment_heading->id;
        $engagment->title 					= "With a detailed representation of project results";
        $engagment->personality_type_id 	= PersonalityType::where('short_title', "C")->first()->id;
        $engagment->save();
    }
}
