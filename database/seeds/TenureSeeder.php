<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\Tenure;

class TenureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project	= Project::where('title', "LTEC")->first();

        $tenure 				= new Tenure;
        $tenure->project_id 	= $project->id;
        $tenure->years 			= 2;
        $tenure->short_for_year = 'Year';
        $tenure->turns_per_year = 4;
        $tenure->short_for_turn = 'Quarter';
        $tenure->total_stages   = 8;
        $tenure->save();
    }
}
