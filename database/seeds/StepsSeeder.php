<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\Steps;

class StepsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$project	= Project::where('title', "LTEC")->first();

        $step 				= new Steps;
        $step->project_id	= $project->id;
        $step->title 		= "Access";
        $step->logo         = "access.png";
        $step->quarterly_production_title   = "New areas";
        $step->quarterly_target             = 0.66;
        $step->overall_production_title     = "Total areas";
        $step->overall_target               = 18.8;
        $step->initial_productivity         = 13.80;
        $step->default_value= 0.66;
        $step->sort 		= 1;
        $step->previous_step_id             = 0;
        $step->save();

        $previous_step_id   = $step->id;

        $step 				= new Steps;
        $step->project_id	= $project->id;
        $step->title 		= "Planning";
        $step->logo 		= "planning.png";
        $step->quarterly_production_title   = "Plans per area";
        $step->quarterly_target             = 0.66;
        $step->overall_production_title     = "Total plans";
        $step->overall_target               = 12.6;
        $step->initial_productivity= 9.45;
        $step->default_value= 0.66;
        $step->sort 		= 2;
        $step->previous_step_id             = $previous_step_id;
        $step->save();

        $previous_step_id   = $step->id;

        $step 				= new Steps;
        $step->project_id	= $project->id;
        $step->title 		= "Contract Approval";
        $step->logo 		= "contract.png";
        $step->quarterly_production_title   = "Contracts per plan";
        $step->quarterly_target             = 0.66;
        $step->overall_production_title     = "Total contracts";
        $step->overall_target               = 9.6;
        $step->initial_productivity= 6.54;
        $step->default_value= 0.66;
        $step->sort 		= 3;
        $step->previous_step_id             = $previous_step_id;
        $step->save();

        $previous_step_id   = $step->id;

        $step 				= new Steps;
        $step->project_id	= $project->id;
        $step->title 		= "Installation";
        $step->logo 		= "installation.png";
        $step->quarterly_production_title   = "Array per contract";
        $step->quarterly_target             = 0.66;
        $step->overall_production_title     = "Total arrays";
        $step->overall_target               = 7.2;
        $step->initial_productivity= 4.09;
        $step->default_value= 0.66;
        $step->sort 		= 4;
        $step->previous_step_id             = $previous_step_id;
        $step->save();

        $previous_step_id   = $step->id;

        $step 				= new Steps;
        $step->project_id	= $project->id;
        $step->title 		= "Operation";
        $step->logo 		= "operation.png";
        $step->quarterly_production_title   = "Energy per array(GWh)";
        $step->quarterly_target             = 160;
        $step->overall_production_title     = "Total energy(GWh)";
        $step->overall_target               = 7000;
        $step->initial_productivity= 0.00;
        $step->default_value= 0.00;
        $step->sort 		= 5;
        $step->previous_step_id             = $previous_step_id;
        $step->save();
    }
}
