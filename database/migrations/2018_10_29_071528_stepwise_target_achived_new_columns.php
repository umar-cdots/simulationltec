<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StepwiseTargetAchivedNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stepwise_target_achiveds', function (Blueprint $table) {
            $table->dropColumn(['game_character_id', 'target_achived', 'stage_no']);
            $table->integer('game_play_id')->after('id');
            $table->integer('game_stage_id')->after('game_play_id');
            $table->integer('leader_id')->after('game_stage_id');
            $table->float('initial_value')->after('step_id');
            $table->float('quarterly_productivity')->after('initial_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stepwise_target_achiveds', function (Blueprint $table) {
            $table->integer('game_character_id');
            $table->integer('target_achived');
            $table->integer('stage_no');
            $table->dropColumn(['game_play_id', 'game_stage_id', 'leader_id', 'initial_value', 'quarterly_productivity']);
        });
    }
}
