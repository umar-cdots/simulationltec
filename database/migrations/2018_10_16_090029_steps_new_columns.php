<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StepsNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('steps', function (Blueprint $table) {
            $table->float('default_value')->after('logo');
            $table->float('initial_productivity')->after('default_value');
            // $table->float('initial_value')->after('initial_productivity');
            $table->integer('sort')->after('initial_productivity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('steps', function (Blueprint $table) {
            $table->dropColumn(['default_value', 'initial_value', 'sort']);
        });
    }
}
