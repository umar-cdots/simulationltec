<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewStepwiseTargetColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stepwise_targets', function (Blueprint $table) {
            $table->dropColumn(['target_title', 'target']);
            $table->integer('step_id')->after('id');
            $table->integer('stage_no')->after('step_id')->comment("stage_no same as game_stages table's stage_no");
            $table->double('target_value')->after('stage_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stepwise_targets', function (Blueprint $table) {
            $table->string('target_title');
            $table->float('target');
            $table->dropColumn(['stage_no', 'target_value']);
        });
    }
}
