<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GameStagewiseDataNewColumn2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_stagewise_datas', function (Blueprint $table) {
            $table->boolean('engaged_after_observing')->default(false)->after('is_observed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_stagewise_datas', function (Blueprint $table) {
            $table->dropColumn('engaged_after_observing');
        });
    }
}
