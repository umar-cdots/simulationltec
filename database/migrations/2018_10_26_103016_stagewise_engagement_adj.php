<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StagewiseEngagementAdj extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('game_stepwise_engagements', 'game_stagewise_engagements');
        Schema::table('game_stagewise_engagements', function (Blueprint $table) {
            $table->integer('game_stage_id')->after('game_play_id');
        });
        Schema::rename('game_stagewise_data', 'game_stagewise_datas');        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_stagewise_engagements', function (Blueprint $table) {
            $table->dropColumn(['game_play_id']);
        });
        Schema::rename('game_stagewise_engagements', 'game_stepwise_engagements');
        Schema::rename('game_stagewise_datas', 'game_stagewise_data');
    }
}
