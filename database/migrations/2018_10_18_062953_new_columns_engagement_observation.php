<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewColumnsEngagementObservation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('engagement_observations', function (Blueprint $table) {
            $table->dropColumn(['description']);
            $table->integer('observation_id')->after('engagement_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('engagement_observations', function (Blueprint $table) {
            $table->text('description')->after('engagement_id');
            $table->dropColumn(['observation_id']);
        });
    }
}
