<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewStepWiseTargetColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stepwise_engagements', function (Blueprint $table) {
            $table->integer('project_id')->after('id');
            $table->integer('game_play_id')->after('project_id');
            $table->integer('game_character_id')->after('game_play_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stepwise_engagments', function (Blueprint $table) {
            $table->dropColumn(['project_id']);
        });
    }
}
