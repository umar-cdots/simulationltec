<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GameStagewiseDataNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_stagewise_datas', function (Blueprint $table) {
            $table->boolean('is_observed')->default(false)->after('observation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_stagewise_datas', function (Blueprint $table) {
            $table->dropColumn('is_observed');
        });
    }
}
