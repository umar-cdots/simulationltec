<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GameStepWiseAdj extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('game_stagewise_observations', 'game_stagewise_data');
        Schema::table('game_stagewise_data', function (Blueprint $table) {
            $table->integer('personality_type_id')->after('leader_id');
        });

        Schema::rename('stepwise_engagements', 'game_stepwise_engagements');
        Schema::table('game_stepwise_engagements', function(Blueprint $table) {
            $table->dropColumn(['game_character_id', 'step_id']);
            $table->integer('leader_id')->after('game_play_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('game_stagewise_data', 'game_stagewise_observations ');
        Schema::table('game_stagewise_observations ', function (Blueprint $table) {
            $table->dropColumn(['personality_type_id']);
        });

        Schema::rename('game_stepwise_engagements', 'stepwise_engagments');
        Schema::table('stepwise_engagments', function(Blueprint $table) {
            $table->integer('game_character_id');
            $table->integer('step_id');
        });
    }
}
