<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewStepsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('steps', function (Blueprint $table) {
            $table->string('quarterly_production_title')->after('logo');
            $table->float('quarterly_target')->after('quarterly_production_title');
            $table->string('overall_production_title')->after('quarterly_target');
            $table->string('overall_target')->after('overall_production_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('steps', function (Blueprint $table) {
            $table->dropColumn(['quarterly_production_title', 'quarterly_target', 'overall_production_title', 'overall_target']);
        });
    }
}
