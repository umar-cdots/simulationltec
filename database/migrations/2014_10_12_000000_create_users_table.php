<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('nick_name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('personality_type_id')->nullable();
            $table->enum('gender', ['male', 'female', 'undefined'])->nullable();
            $table->string('education_level')->nullable();
            $table->string('organization_type')->nullable();
            $table->string('organization_size')->nullable();
            $table->string('industry')->nullable();
            $table->string('function')->nullable();
            $table->string('career_level')->nullable();
            $table->string('working_experience')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
