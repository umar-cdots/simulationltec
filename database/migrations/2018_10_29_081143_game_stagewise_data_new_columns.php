<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GameStagewiseDataNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_stagewise_datas', function (Blueprint $table) {
            $table->float('leadership_effectiveness')->after('observation_id')->nullable();
            $table->float('initial_value')->after('leadership_effectiveness')->nullable();
            $table->float('productivity')->after('initial_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_stagewise_datas', function (Blueprint $table) {
            $table->dropColumn(['leadership_effectiveness', 'initial_value', 'productivity']);
        });
    }
}
