<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewStepwiseTargetAchived extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stepwise_target_achiveds', function (Blueprint $table) {
            $table->integer('game_character_id')->after('id');
            $table->integer('step_id')->after('game_character_id');
            $table->integer('stage_no')->after('step_id');
            $table->integer('target_achived')->after('step_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stepwise_target_achiveds', function (Blueprint $table) {
            $table->dropColumn([]);
        });
    }
}
