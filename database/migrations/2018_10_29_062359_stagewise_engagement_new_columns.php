<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StagewiseEngagementNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_stagewise_engagements', function (Blueprint $table) {
            $table->enum('accuracy', ['correct', 'opposite', 'wrong'])->after('engagment_id');
            $table->float('points_earned')->after('accuracy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_stagewise_engagements', function (Blueprint $table) {
            $table->dropColumn(['accuracy', 'points_earned']);
        });
    }
}
