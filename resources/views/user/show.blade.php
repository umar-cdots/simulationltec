@extends('adminlte::page')

@section('title', 'SIMMETRIC:GAIA')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Users</h1>
		</section>
		<section class="content">
			<div class="box">
	            <div class="box-body">
	            	<table class="table table-bordered table-hover">
	            		<tr>
	            			<td class="boldIt">Name</td>
	            			<td>{{ $user->name }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Email</td>
	            			<td>{{ $user->email }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Gender</td>
	            			<td>{{ $user->gender }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Education Level</td>
	            			<td>{{ $user->education_level }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Organization Type</td>
	            			<td>{{ $user->organization_type }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Organization Size</td>
	            			<td>{{ $user->organization_size }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Industry</td>
	            			<td>{{ $user->industry }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Function</td>
	            			<td>{{ $user->function }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Career Level</td>
	            			<td>{{ $user->career_level }}</td>
	            		</tr>
	            		<tr>
	            			<td class="boldIt">Working Experience</td>
	            			<td>{{ $user->working_experience }}</td>
	            		</tr>
	            		<tr>
	            			<td colspan="2"></td>
	            		</tr>
	            	</table>
	            </div>
	        </div>
	    </section>
@endsection
@push('css')
	<style type="text/css">
		.boldIt{
			font-weight: bold;
		}
	</style>
@endpush