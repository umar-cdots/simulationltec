@extends('adminlte::page')

@section('title', 'Update Profile - SIMMETRIC:GAIA')

@section('content_header')
<h1>Update Profile</h1>
<br>
@endsection

@section('content')
<div class="row">
      <div class="col-md-12">
        <div class="col-md-offset-3 col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{ route('user.save', $user->id) }}" method="post">
             @csrf
             @method('PUT')
              <div class="box-body">
                <div class="form-group {{ $errors->has('nick_name') ? 'has-error' : ''}}">
                  <label for="phone">Nickname</label>
                  <input type="text" class="form-control" name="nick_name" id="nick_name" placeholder="Enter Nickname" value="{{ old('nick_name', $user->nick_name) }}">
                  @if ($errors->has('nick_name'))
                    <span class="help-block text-danger">{{ $errors->first('nick_name') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('personality_type_id') ? 'has-error' : ''}}">
                  <label for="personality_type_id">Your main DISC style (Please leave this field empty if you have never done a DISC assessment or if you don't remember your style)</label>
                  <br>
                  @foreach($personalityTypes as $personalityType)
                    <label class="radio-inline">
                      <input type="radio" name="personality_type_id" value="{{ $personalityType->id }}" {{ $personalityType->id == old('personality_type_id', $user->personality_type_id) ? 'checked' : '' }}>{{ $personalityType->short_title }}
                    </label>
                  @endforeach
                  @if ($errors->has('personality_type_id'))
                    <span class="help-block text-danger">{{ $errors->first('personality_type_id') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                  <label for="gender">Gender</label>
                  <br>
                  <label class="radio-inline">
                    <input type="radio" name="gender" value="male" {{ "male" == old('gender', $user->gender) ? 'checked' : '' }}>Male
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="gender" value="female" {{ "male" == old('gender', $user->gender) ? 'checked' : '' }}>Female
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="gender" value="undefined" {{ "undefined" == old('gender', $user->gender) ? 'checked' : '' }}>Prefer not to say
                  </label>
                  @if ($errors->has('gender'))
                    <span class="help-block text-danger">{{ $errors->first('gender') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('education_level') ? 'has-error' : ''}}">
                  <label for="education_level">Education Level</label>
                  <select class="form-control" id="education_level" name="education_level">
                    <option value="">Select Education Level</option>
                    <option value="No Degree" {{ old('education_level', $user->education_level) == "No Degree" ? 'selected' : ''}}>No Degree</option>
                    <option value="Bachelor" {{ old('education_level', $user->education_level) == "Bachelor" ? 'selected' : ''}}>Bachelor</option>
                    <option value="Master" {{ old('education_level', $user->education_level) == "Master" ? 'selected' : ''}}>Master</option>
                    <option value="PhD" {{ old('education_level', $user->education_level) == "PhD" ? 'selected' : ''}}>PhD</option>
                  </select>
                  @if ($errors->has('education_level'))
                    <span class="help-block text-danger">{{ $errors->first('education_level') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('organization_type') ? 'has-error' : ''}}">
                  <label for="organization_type">Organization Type</label>
                  <select class="form-control" id="organization_type" name="organization_type">
                    <option value="">Select Organization Type</option>
                    <option value="Private" {{ old('organization_type', $user->organization_type) == "Private" ? 'selected' : ''}}>Private</option>
                    <option value="Semi-gov" {{ old('organization_type', $user->organization_type) == "Semi-gov" ? 'selected' : ''}}>Semi-gov</option>
                    <option value="Gov" {{ old('organization_type', $user->organization_type) == "Gov" ? 'selected' : ''}}>Gov</option>
                    <option value="NGO" {{ old('organization_type', $user->organization_type) == "NGO" ? 'selected' : ''}}>NGO</option>
                  </select>
                  @if ($errors->has('organization_type'))
                    <span class="help-block text-danger">{{ $errors->first('organization_type') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('organization_size') ? 'has-error' : ''}}">
                  <label for="organization_size">Organization Size</label>
                  <select class="form-control" id="organization_size" name="organization_size">
                    <option value="">Select Organization Size</option>
                    <option value="Below 100" {{ old('organization_size', $user->organization_size) == "Below 100" ? 'selected' : ''}}>Below 100</option>
                    <option value="100-500" {{ old('organization_size', $user->organization_size) == "100-500" ? 'selected' : ''}}>100-500</option>
                    <option value="500-1000" {{ old('organization_size', $user->organization_size) == "500-1000" ? 'selected' : ''}}>500-1000</option>
                    <option value="Above 1000" {{ old('organization_size', $user->organization_size) == "Above 1000" ? 'selected' : ''}}>Above 1000</option>
                  </select>
                  @if ($errors->has('organization_size'))
                    <span class="help-block text-danger">{{ $errors->first('organization_size') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('industry') ? 'has-error' : ''}}">
                  <label for="industry">Industry</label>
                  <select class="form-control" id="industry" name="industry[]" multiple="">
                    <option value="Automotive & Mobility" {{ old('industry') == "Automotive & Mobility" ? 'selected' : ''}}>Automotive & Mobility</option>
                    <option value="Biopharma & Health Care" {{ old('industry') == "Biopharma & Health Care" ? 'selected' : ''}}>Biopharma & Health Care</option>
                    <option value="Consumer Products" {{ old('industry') == "Consumer Products" ? 'selected' : ''}}>Consumer Products</option>
                    <option value="Education" {{ old('industry') == "Education" ? 'selected' : ''}}>Education</option>
                    <option value="Energy & Environment" {{ old('industry') == "Energy & Environment" ? 'selected' : ''}}>Energy & Environment</option>
                    <option value="Engineered products & Infrastructure" {{ old('industry') == "Engineered products & Infrastructure" ? 'selected' : ''}}>Engineered products & Infrastructure</option>
                    <option value="Financial Institutions" {{ old('industry') == "Financial Institutions" ? 'selected' : ''}}>Financial Institutions</option>
                    <option value="Media & Entertainment" {{ old('industry') == "Media & Entertainment" ? 'selected' : ''}}>Media & Entertainment</option>
                    <option value="Technology" {{ old('industry') == "Technology" ? 'selected' : ''}}>Technology</option>
                    <option value="Metals & Mining" {{ old('industry') == "Metals & Mining" ? 'selected' : ''}}>Metals & Mining</option>
                    <option value="Principal Investors & Private Equity" {{ old('industry') == "Principal Investors & Private Equity" ? 'selected' : ''}}>Principal Investors & Private Equity</option>
                    <option value="Process Industries & Building Materials" {{ old('industry') == "Process Industries & Building Materials" ? 'selected' : ''}}>Process Industries & Building Materials</option>
                    <option value="Professional Services" {{ old('industry') == "Professional Services" ? 'selected' : ''}}>Professional Services</option>
                    <option value="Public Sector" {{ old('industry') == "Public Sector" ? 'selected' : ''}}>Public Sector</option>
                    <option value="Retail" {{ old('industry') == "Retail" ? 'selected' : ''}}>Retail</option>
                    <option value="Social Impact" {{ old('industry') == "Social Impact" ? 'selected' : ''}}>Social Impact</option>
                    <option value="Telecommunications" {{ old('industry') == "Telecommunications" ? 'selected' : ''}}>Telecommunications</option>
                    <option value="Transportation, Travel & Tourism" {{ old('industry') == "Transportation, Travel & Tourism" ? 'selected' : ''}}>Transportation, Travel & Tourism</option>
                    <option value="Other" {{ old('industry') == "Other" ? 'selected' : ''}}>Other</option>
                  </select>
                  @if ($errors->has('industry'))
                    <span class="help-block text-danger">{{ $errors->first('industry') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('function') ? 'has-error' : ''}}">
                  <label for="function">Function</label>
                  <select class="form-control" id="function" name="function[]" multiple="">
                    <option value="Business unit" {{ old('function') == "Business unit" ? 'selected' : ''}}>Business unit</option>
                    <option value="Consulting" {{ old('function') == "Consulting" ? 'selected' : ''}}>Consulting</option>
                    <option value="HR" {{ old('function') == "HR" ? 'selected' : ''}}>HR</option>
                    <option value="Legal" {{ old('function') == "Legal" ? 'selected' : ''}}>Legal</option>
                    <option value="Finance" {{ old('function') == "Finance" ? 'selected' : ''}}>Finance</option>
                    <option value="Planning" {{ old('function') == "Planning" ? 'selected' : ''}}>Planning</option>
                    <option value="Procurement" {{ old('function') == "Procurement" ? 'selected' : ''}}>Procurement</option>
                    <option value="Sales" {{ old('function') == "Sales" ? 'selected' : ''}}>Sales</option>
                    <option value="Marketing" {{ old('function') == "Marketing" ? 'selected' : ''}}>Marketing</option>
                    <option value="Other" {{ old('function') == "Other" ? 'selected' : ''}}>Other</option>
                  </select>
                  @if ($errors->has('function'))
                    <span class="help-block text-danger">{{ $errors->first('function') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('career_level') ? 'has-error' : ''}}">
                  <label for="career_level">Career Level</label>
                  <select class="form-control" id="career_level" name="career_level">
                    <option value="">Select Career Level</option>
                    <option value="Individual Contributor" {{ old('career_level', $user->career_level) == "Individual Contributor" ? 'selected' : ''}}>Individual Contributor</option>
                    <option value="Entry level manager" {{ old('career_level', $user->career_level) == "Entry level manager" ? 'selected' : ''}}>Entry level manager</option>
                    <option value="Middle manager" {{ old('career_level', $user->career_level) == "Middle manager" ? 'selected' : ''}}>Middle manager</option>
                    <option value="Senior manager" {{ old('career_level', $user->career_level) == "Senior manager" ? 'selected' : ''}}>Senior manager</option>
                    <option value="CXO" {{ old('career_level', $user->career_level) == "CXO" ? 'selected' : ''}}>CXO</option>
                  </select>
                  @if ($errors->has('career_level'))
                    <span class="help-block text-danger">{{ $errors->first('career_level') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('working_experience') ? 'has-error' : ''}}">
                  <label for="working_experience">Working Experience</label>
                  <select class="form-control" id="working_experience" name="working_experience">
                    <option value="">Select Working Experience</option>
                    <option value="Below 5 years" {{ old('working_experience', $user->working_experience) == "Below 5 years" ? 'selected' : ''}}>Below 5 years</option>
                    <option value="5-10 years" {{ old('working_experience', $user->working_experience) == "5-10 years" ? 'selected' : ''}}>5-10 years</option>
                    <option value="10-20 years" {{ old('working_experience', $user->working_experience) == "10-20 years" ? 'selected' : ''}}>10-20 years</option>
                    <option value="Above 20 years" {{ old('working_experience', $user->working_experience) == "Above 20 years" ? 'selected' : ''}}>Above 20 years</option>
                  </select>
                  @if ($errors->has('working_experience'))
                    <span class="help-block text-danger">{{ $errors->first('working_experience') }}</span>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        </div>
      </div>
@endsection

@push('js')

@endpush