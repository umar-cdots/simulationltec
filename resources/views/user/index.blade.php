@extends('adminlte::page')

@section('title', 'SIMMETRIC:GAIA')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
			<h1 class="pull-left">Users</h1>
			<a href="{{ route('user.create') }}">
				<button class="btn btn-primary pull-right">Add New</button>
			</a>
			<div class="clearfix"></div>
		</section>
		<br>
		<section class="content">
			<div class="box">
	            <div class="box-body">
	            	<form class="form-inline" action="{{ route('user.index') }}" method="GET">
	            		<div class="form-group">
      						<label for="email">Sessions:</label>
	            			<select class="form-control" name="session_id">
	            				<option value="">All Sessions</option>
	            				@foreach($sessions as $session)
	            					<option value="{{ $session->id }}" {{ $session->id == request('session_id') ? 'selected' : '' }}>{{ $session->title }}</option>
	            				@endforeach
	            			</select>
	            		</div>
	            		<button type="submit" class="btn btn-primary">Go</button>
	            	</form>
	            	<table class="table table-bordered table-striped" id="table">
	            		<thead>
	            			<tr>
	            				<th>Sr#</th>
	            				<th>Name</th>
	            				<th>Email Address</th>
	            				<th>Project</th>
	            				<th>Session</th>
	            				<th>Total Attempts</th>
	            				<th>Action</th>
	            			</tr>
	            		</thead>
	            		<tbody>
	            			@foreach($users as $user)
		            			<tr>
		            				<td>{{ $loop->iteration }}</td>
		            				<td>{{ $user->name }}</td>
		            				<td>{{ $user->email }}</td>
		            				<td>{{ $user->session->project->title }}</td>
		            				<td>{{ $user->session->title }}</td>
		            				<td>{{ $user->gamePlays->count() }}</td>
		            				<td>
		            					{{-- <a href="{{ route('user.show', $user->id) }}" class="btn btn-xs btn-success" title="View"><i class="fa fa-eye"></i></a> --}}
		            					{{-- <a href="{{ route('user.edit', $user->id) }}" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a> --}}
		            					<form action="{{ route('user.destroy', $user->id) }}" method="POST" style="display: inline;">
										    <input type="hidden" name="_method" value="DELETE">
										    <input type="hidden" name="_token" value="{{ csrf_token() }}">
										    <button type="submit" class="btn btn-xs btn-danger remove" title="Remove"><i class="fa fa-remove"></i></button>
										</form>
		            					{{-- <a href="" class="btn btn-xs btn-danger remove" title="Remove"><i class="fa fa-remove"></i></a> --}}
		            				</td>
		            			</tr>
	            			@endforeach
	            		</tbody>
	            	</table>
	            </div>
	        </div>
		</section>
	</div>
@endsection

@push('js')
<script type="text/javascript">
	$('#table').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
</script>
@endpush