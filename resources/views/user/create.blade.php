@extends('adminlte::page')

@section('title', 'New User - SIMMETRIC:GAIA')

@section('content_header')
<h1>New User</h1>
<br>
@endsection

@section('content')
<div class="row">
      <div class="col-md-12">
        <div class="col-md-offset-3 col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add User Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{ route('user.store') }}" method="post">
             @csrf
              <div class="box-body">
                {{-- <div class="form-group {{ $errors->has('project_id') ? 'has-error' : ''}}">
                  <label for="name">Project</label>
                  <select class="form-control" id="project_id" name="project_id">
                    <option value="">Select Project</option>
                    @foreach ($projects as $project)
                     <option value="{{$project->id}}" {{ $project->id == old('project_id') ? 'selected' : ''}}>{{$project->title }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('project_id'))
                    <span class="help-block text-danger">{{ $errors->first('project_id') }}</span>
                  @endif
                </div> --}}
                <div class="form-group {{ $errors->has('session_id') ? 'has-error' : ''}}">
                  <label for="name">Session</label>
                  <select class="form-control" id="session_id" name="session_id">
                    <option value="">Select a Session</option>
                    @foreach ($sessions as $session)
                     <option value="{{$session->id}}" {{ $session->id == old('session_id') ? 'selected' : ''}}>{{$session->title }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('session_id'))
                    <span class="help-block text-danger">{{ $errors->first('session_id') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label for="phone">Name</label>
                  <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="{{ old('name') }}">
                  @if ($errors->has('name'))
                    <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                  <label for="phone">Email</label>
                  <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email Address" value="{{ old('email') }}">
                  @if ($errors->has('email'))
                    <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                  <label for="phone">Password</label>
                  <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" value="">
                  @if ($errors->has('password'))
                    <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                  <label for="phone">Confirm Password</label>
                  <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Enter Confirm Password" value="">
                  @if ($errors->has('password_confirmation'))
                    <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        </div>
      </div>
@endsection

@push('js')

@endpush