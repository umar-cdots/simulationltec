@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    @stack('css')
    @yield('css')
    <style type="text/css">
        .navbar-brand
        {
            height: auto;
        }
        .navbar-header 
        {
            float: left;
            width: 15%;
        }
        .navbar-brand > img 
        {
            display: block;
            width: 100%;
            padding-top: 12px;
            padding-right: 10px;
        }
        .navbar-header .navbar-brand
        {
            margin: 0px !important;
            padding: 0px !important;
        }
        nav.navbar
        {
            background-color: #ffffff !important;
        }
        nav.navbar a
        {
            color: #29ba74 !important;
        }
        .content-wrapper
        {
            background-image: url("/img/bg.jpg");
        }
        li.sub-legend a
        {
            color: #FFF !important;
            border-radius: 0px;
        }
        li.sub-legend a:hover
        {
            opacity: 0.9;
        }
        li.sub-legend.increasing a
        {
            background-color: #29ba74;
        }
        li.sub-legend.stable a
        {
            background-color: #d4df33;
        }
        li.sub-legend.decreasing a
        {
            background-color: #e71c57;
        }
    </style>
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                            <img src="{{ asset('img/m-logos.png') }}">
                            {{-- {!! config('adminlte.logo', '<b>Admin</b>LTE') !!} --}}
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
            @else
            <!-- Logo -->
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
            @endif
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        @if(\Auth::user()->user_type == 'user')
                            <li class="dropdown help-dropdown" style="display: none;">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-question-circle"></i>
                                    Help
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu help-menu" role="menu">
                                    <li>
                                        <a href="#" id="loadIntro">
                                            <i class="fa fa-info-circle go-intro"></i>
                                            Intro
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="$('#legendModal').modal('show')">
                                            <i class="fa fa-list-ul go-legend"></i>
                                            Legend
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="loadTutorial">
                                            <i class="fa fa-paperclip go-tutorial"></i>
                                            Tutorial
                                        </a>
                                    </li>
                                </ul>
                            </li>                        
                        @endif
                        <li>
                            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                            @else
                                <a href="#"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                >
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                    @if(config('adminlte.logout_method'))
                                        {{ method_field(config('adminlte.logout_method')) }}
                                    @endif
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </li>
                    </ul>
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                      <img src="{{ asset('vendor/adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                      <p>{{ Auth::user()->name }}</p>
                      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- Sidebar Menu -->
                <ul class="sidebar-menu" data-widget="tree">
                    @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>
        @endif

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
            <div class="container-fluid">
            @endif

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <section class="content">

                @yield('content')

            </section>
            <!-- /.content -->
            @if(config('adminlte.layout') == 'top-nav')
            </div>
            <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
    <script type="text/javascript">
        @if(Auth::user()->user_type == 'user')
            if($('.nav li.active').length > 1)
            {
                $('.nav li.active:first-child').removeClass('active');
            }
            $('i.pa_anchor').parent().attr('href', "{{ route('performance_analysis',  \App\GamePlay::where('user_id', Auth::id())->orderBy('id', 'DESC')->count() > 0 ? \App\GamePlay::where('user_id', Auth::id())->orderBy('id', 'DESC')->first()->id : '') }}");
        @endif
        @if (session('response'))
            toastr['{{ session('response')['status'] }}']("{{ session('response')['message'] }}");
        @endif
    </script>
@stop
