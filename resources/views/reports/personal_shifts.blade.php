@extends('adminlte::page')

@section('title', 'SIMMETRIC:GAIA')

@section('content')
<div class="content-header">
	<div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Suggested shifts based on your performance</h3>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	<div class="box box-solid">
        		<div class="container-fluid">
	        		<p>
	        			{{ $message ?? '' }}
	        		</p>
	        	</div>
        	</div>
        	<div class="clearfix"></div>
        	<div class="container-fluid">
        		<div class="col-md-12">
	        		<div class="col-md-1 heading_center">
	        			<h4>Style</h4>
	        		</div>
	        		<div class="col-md-offset-1 col-md-3 heading_center">
	        			<h4>Name</h4>
	        		</div>
	        		<div class="col-md-offset-1 col-md-6 heading_center">
	        			<h4>Description</h4>
	        		</div>
	        		<div class="clearfix"></div>
	        	</div>
	        	@foreach($personal_shifts as $personal_shift)
	        		<div class="col-md-12 margin-bottom">
	        			<div class="col-md-1 diagnose diagnose-{{ strtolower($personal_shift->personalityType->short_title) }}">
	        				<h3>{{ $personal_shift->personalityType->short_title }}</h3>
	        			</div>
	        			<div class="col-md-offset-1 col-md-3">
	        				<p>
	        					{{ $personal_shift->name }}
	        				</p>
	        			</div>
	        			<div class="col-md-offset-1 col-md-6">
	        				<p>
	        					{{ $personal_shift->description }}
	        				</p>
	        			</div>
	        		</div>
	        	@endforeach
	        </div>
        </div>
        <!-- /.box-body -->
  	</div>
</div>
@endsection

@push('css')
<style type="text/css">
	.top-download
	{
		margin-bottom: 10px;
		padding-right: 0px;
	}
	.btn-download
	{
		background-color: #29ba74;
		border-color: #29ba74;
		color: #fff;
	}
	.btn-download:hover, .btn-download:active
	{
		color: #fff;
	}
	.box-report
	{
		border: 1px solid #9a9a9a !important;
	}
	.box-report .box-header
	{
		color: #fff !important;
		background-color: #9a9a9a !important;
	}
	.progress-bar-report
	{
		background: #29ba74 !important;
		font-size: 20px !important;
		line-height: 40px !important;
	}
	.progress
	{
		height: 40px !important;
	}
	.margin-bottom
	{
		margin-bottom: 10px;
	}
	.margin-bottom img
	{
		margin-right: 3px;
	}
	.margin-bottom h4
	{
		line-height: 20px;
	}
	h4.leader-name
	{
		font-size: 17px;
		max-width: 125px;
		white-space: nowrap;
	}
	.diagnose-d
	{
		background-color: #f57c00;
	}
	.diagnose-i
	{
		background-color: #afb42b; 
	}
	.diagnose-s
	{
		background-color: #ffc107;
	}
	.diagnose-c
	{
		background-color: #ffa000;
	}
	.diagnose-correct
	{
		background-color: #29ba74;
	}
	.diagnose-wrong
	{
		background-color: #d4df33;
	}
	.diagnose-opposite
	{
		background-color: #e71c57;
	}
	.diagnose h3
	{
		margin-top: 10px;
		text-align: center;
	}
	.diagnose
	{
		border-right: 5px solid #fff;
	}
	.heading_center
	{
		text-align: center;
	}
	.progress-bar
	{
		color: #000;
	}
	.content-wrapper
	{
		background-color: #FFF;
	}
</style>
@endpush

@push('js')
@endpush