@extends('adminlte::page')

@section('title', 'SIMMETRIC:GAIA')

@section('content')
<div class="content-header">
	<div class="container-fluid top-download" style="margin-bottom: 10px;">
		<button class="btn pull-right btn-download" id="download"><i class="fa fa-download"></i> Download</button>
		<div class="clearfix"></div>
	</div>
	<div class="container-fluid pdf-logo" style="margin-bottom: 10px; padding: 0; display: none;">
		<div class="col-md-12" style="padding: 0;">
			<div class="col-md-6">
				<img src="{{ asset('img/m-logos.png') }}" style="width: 40%;">
			</div>
			<div class="col-md-6 text-right">
				<h1 style="margin: 0px;">Comparative Analysis</h1>
			</div>
			<div class="clearfix"></div>
			<table class="table table-bordered pdf-heading" style="margin-top: 10px; margin-bottom: 0;">
				<tbody>
					<tr>
						<th>Name</th>
						<td>{{ Auth::user()->name }}</td>
						<th>Email</th>
						<td>{{ Auth::user()->email }}</td>
						<th>Date</th>
						<td id="sys_date"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="clearfix"></div>
    <div class="box box-solid box-report">
        <div class="box-header with-border text-center">
          <h3 class="box-title">Number of attempts: <span>{{ $game_plays->count() }}</span></h3>
          <!-- /.box-tools -->
        </div>
    </div>
	<div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Overall performance</h3>
          <!-- /.box-tools -->
        </div>
	    <!-- /.box-header -->
	    <div class="box-body">
	    	<div class="box box-solid">
	    		<div class="container">
	        		<p id="overall_performance_text"></p>
	        	</div>
	        </div>
	    	<div class="clearfix"></div>
	    	<div class="col-md-12">
	    		<div id="overall_performance_chart"></div>
	    		<div id="overall_performance_img" style="display: none;"></div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="class-md-12" style="overflow-x: scroll;" id="overall_performance_stats">
	    		@php
	    			$uo_best 		= 0;
	    			$uo_last_value	= 0;
	    			$ao_best 		= 0;
	    			$ao_last_value	= 0;
	    		@endphp
	    		<table class="table table-hover table-bordered">
	    			<tbody>
	    				{{-- <tr>
	    					<td>Approach Quality</td>
	    					@foreach($per_approach_quality_arr as $value)
	    						<td>{{ $value }}%</td>
	    					@endforeach
	    				</tr> --}}
	    				<tr>
	    					<td>Understanding Others</td>
	    					@foreach($per_understanding_others_arr as $value)
	    						<td>{{ $value }}%</td>
	    						@if ($loop->last)
	    							@php
	    								$uo_last_value = $value;
	    							@endphp
	    						@else
	    							@php
	    								if($value > $uo_best):
	    									$uo_best = $value;
	    								endif;
	    							@endphp
	    						@endif
	    					@endforeach
	    				</tr>
	    				<tr>
	    					<td>Adapting to Others</td>
	    					@foreach($per_adapting_to_others_arr as $value)
	    						<td>{{ $value }}%</td>
	    						@if ($loop->last)
	    							@php
	    								$ao_last_value = $value;
	    							@endphp
	    						@else
	    							@php
	    								if($value > $ao_best):
	    									$ao_best = $value;
	    								endif;
	    							@endphp
	    						@endif
	    					@endforeach
	    				</tr>
	    			</tbody>
	    		</table>
	    		@push('js')
	    			<script type="text/javascript">
	    				var final_text = "Compared to your best performance, ";
	    				@if($uo_last_value > $uo_best)
	    					final_text += "you have understood personal styles better ";
	    				@elseif($uo_last_value < $uo_best)
	    					final_text += "you have understood personal styles less effectively ";
	    				@else
	    					final_text += "you have maintained the same level of understanding of the personal styles ";
	    				@endif
	    				@if($ao_last_value > $ao_best)
	    					final_text += "and you have flexed your style more effectively.";
	    				@elseif($ao_last_value < $ao_best)
	    					final_text += "and you have flexed your style less effectively.";
	    				@else
	    					final_text += "and you have flexed your style in the same way.";
	    				@endif
	    				$('#overall_performance_text').text(final_text);
	    			</script>
	    		@endpush
	    	</div>
	    </div>
    </div>
	<div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Approach quality</h3>
          <!-- /.box-tools -->
        </div>
	    <!-- /.box-header -->
	    <div class="box-body">
	    	{{-- <div class="box box-solid">
	    		<div class="container">
	        		<p>
	        			Compared to your best performance, you have made less decisions after observing behaviors, you haven't changed the amount of diagnosis conducted, and you have engaged your direct reports a comparable amount of times. Compared to your average performance, you have made less decisions after observing behaviors, you haven't changed the amount of diagnosis conducted, and you have engaged your direct reports a comparable amount of times.
	        		</p>
	        	</div>
	        </div>
	    	<div class="clearfix"></div> --}}
	    	<div class="col-md-12">
	    		<div id="approach_quality_chart"></div>
	    		<div id="approach_quality_img" style="display: none;"></div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="class-md-12" style="overflow-x: scroll;" id="approach_quality_stats">
	    		<table class="table table-hover table-bordered">
	    			<tbody>
	    				<tr>
	    					<td>Decisions made after observing behaviors (%)</td>
	    					@foreach($per_descisions_made_arr as $value)
	    						<td>{{ $value }}%</td>
	    					@endforeach
	    				</tr>
	    				<tr>
	    					<td>Diagnosis made (%)</td>
	    					@foreach($per_potential_diagnosis_arr as $value)
	    						<td>{{ $value }}%</td>
	    					@endforeach
	    				</tr>
	    				<tr>
	    					<td>Engagements made (%)</td>
	    					@foreach($per_potential_engagements_arr as $value)
	    						<td>{{ $value }}%</td>
	    					@endforeach
	    				</tr>
	    			</tbody>
	    		</table>
	    	</div>
	    </div>
    </div>
	<div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Performance with specific profiles</h3>
          <!-- /.box-tools -->
        </div>
	    <!-- /.box-header -->
	    <div class="box-body">
	    	<div class="box box-solid">
	    		<div class="container-fluid performance">
        			<div class="col-md-1 diagnose diagnose-d margin-bottom">
        				<h4>D</h4>
        			</div>
        			<div class="col-md-11">
		        		<p id="d-text"></p>
		        	</div>
		        	<div class="clearfix"></div>
        			<div class="col-md-1 diagnose diagnose-i margin-bottom">
        				<h4>I</h4>
        			</div>
        			<div class="col-md-11">
		        		<p id="i-text"></p>
		        	</div>
		        	<div class="clearfix"></div>
        			<div class="col-md-1 diagnose diagnose-s margin-bottom">
        				<h4>S</h4>
        			</div>
        			<div class="col-md-11">
		        		<p id="s-text"></p>
		        	</div>
		        	<div class="clearfix"></div>
        			<div class="col-md-1 diagnose diagnose-c margin-bottom">
        				<h4>C</h4>
        			</div>
        			<div class="col-md-11">
		        		<p id="c-text"></p>
		        	</div>
		        	<div class="clearfix"></div>
	        	</div>
	        </div>
	    	<div class="clearfix"></div>
	    	<div class="col-md-12">
	    		<div id="specific_profiles_chart"></div>
	    		<div id="specific_profiles_img" style="display: none;"></div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="class-md-12" style="overflow-x: scroll;" id="specific_profiles_stats">
	    		@php
	    			$d_best 		= 0;
	    			$d_last_value	= 0;
	    			$i_best	 		= 0;
	    			$i_last_value	= 0;
	    			$s_best 		= 0;
	    			$s_last_value	= 0;
	    			$c_best 		= 0;
	    			$c_last_value	= 0;
	    		@endphp
	    		<table class="table table-hover table-bordered">
	    			<tbody>
	    				<tr>
	    					<td>D</td>
	    					@foreach($per_performance_arr['D'] as $value)
	    						<td>{{ $value }}%</td>
	    						@if ($loop->last)
	    							@php
	    								$d_last_value = $value;
	    							@endphp
	    						@else
	    							@php
	    								if($value > $d_best):
	    									$d_best = $value;
	    								endif;
	    							@endphp
	    						@endif
	    					@endforeach
	    				</tr>
	    				<tr>
	    					<td>I</td>
	    					@foreach($per_performance_arr['I'] as $value)
	    						<td>{{ $value }}%</td>
	    						@if ($loop->last)
	    							@php
	    								$i_last_value = $value;
	    							@endphp
	    						@else
	    							@php
	    								if($value > $i_best):
	    									$i_best = $value;
	    								endif;
	    							@endphp
	    						@endif
	    					@endforeach
	    				</tr>
	    				<tr>
	    					<td>S</td>
	    					@foreach($per_performance_arr['S'] as $value)
	    						<td>{{ $value }}%</td>
	    						@if ($loop->last)
	    							@php
	    								$s_last_value = $value;
	    							@endphp
	    						@else
	    							@php
	    								if($value > $s_best):
	    									$s_best = $value;
	    								endif;
	    							@endphp
	    						@endif
	    					@endforeach
	    				</tr>
	    				<tr>
	    					<td>C</td>
	    					@foreach($per_performance_arr['C'] as $value)
	    						<td>{{ $value }}%</td>
	    						@if ($loop->last)
	    							@php
	    								$c_last_value = $value;
	    							@endphp
	    						@else
	    							@php
	    								if($value > $c_best):
	    									$c_best = $value;
	    								endif;
	    							@endphp
	    						@endif
	    					@endforeach
	    				</tr>
	    			</tbody>
	    		</table>
	    		{{-- <h1>==> {{ die($d_last_value . '|'. $d_sum . '|' . $d_iterations) }}</h1> --}}
	    		@push('js')
	    			<script type="text/javascript">
	    				var d_text = "Compared to your best performance, ";
	    				var i_text = "Compared to your best performance, ";
	    				var s_text = "Compared to your best performance, ";
	    				var c_text = "Compared to your best performance, ";
	    				@if($d_last_value > $d_best)
	    					d_text += "you have improved your understanding of this style.";
	    				@elseif($d_last_value < $d_best)
	    					d_text += "you have demonstrated a poorer understanding of this style.";
	    				@else
	    					d_text += "you haven't improved your understanding of this style.";
	    				@endif
	    				$('#d-text').text(d_text);
	    				@if($i_last_value > $i_best)
	    					i_text += "you have improved your understanding of this style.";
	    				@elseif($i_last_value < $i_best)
	    					i_text += "you have demonstrated a poorer understanding of this style.";
	    				@else
	    					i_text += "you haven't improved your understanding of this style.";
	    				@endif
	    				$('#i-text').text(i_text);
	    				@if($s_last_value > $s_best)
	    					s_text += "you have improved your understanding of this style.";
	    				@elseif($s_last_value < $s_best)
	    					s_text += "you have demonstrated a poorer understanding of this style.";
	    				@else
	    					s_text += "you haven't improved your understanding of this style.";
	    				@endif
	    				$('#s-text').text(s_text);
	    				@if($c_last_value > $c_best)
	    					c_text += "you have improved your understanding of this style.";
	    				@elseif($c_last_value < $c_best)
	    					c_text += "you have demonstrated a poorer understanding of this style.";
	    				@else
	    					c_text += "you haven't improved your understanding of this style.";
	    				@endif
	    				$('#c-text').text(c_text);
	    			</script>
	    		@endpush
	    	</div>
	    </div>
    </div>
</div>
@endsection

@push('css')
<style type="text/css">
	.top-download
	{
		margin-bottom: 10px;
		padding-right: 0px;
	}
	.btn-download
	{
		background-color: #29ba74;
		border-color: #29ba74;
		color: #fff !important;
	}
	.btn-download:hover, .btn-download:active
	{
		color: #fff;
	}
	.box-report
	{
		border: 1px solid #9a9a9a !important;
	}
	.box-report .box-header
	{
		color: #fff !important;
		background-color: #9a9a9a !important;
	}
	.progress-bar-report
	{
		background: #3ead92 !important;
		font-size: 18px !important;
		line-height: 40px !important;
	}
	.progress
	{
		height: 40px !important;
	}
	.margin-bottom
	{
		margin-bottom: 10px;
	}
	.margin-bottom img
	{
		margin-right: 3px;
	}
	.margin-bottom h4
	{
		line-height: 20px;
	}
	h4.leader-name
	{
		font-size: 17px;
		max-width: 125px;
		white-space: nowrap;
	}
	.diagnose-d
	{
		background-color: #f57c00;
	}
	.diagnose-i
	{
		background-color: #afb42b; 
	}
	.diagnose-s
	{
		background-color: #ffc107;
	}
	.diagnose-c
	{
		background-color: #ffa000;
	}
	.diagnose-correct
	{
		background-color: #3ead92;
	}
	.diagnose-wrong
	{
		background-color: #ffff66;
	}
	.diagnose-opposite
	{
		background-color: #f44336;
	}
	.diagnose h3, .diagnose h4
	{
		margin-top: 10px;
		text-align: center;
	}
	.diagnose
	{
		border-right: 5px solid #fff;
	}
	.heading_center
	{
		text-align: center;
	}
	.progress-bar
	{
		color: #000;
	}
	.content-wrapper
	{
		background-color: #FFF;
	}
	p
	{
		font-size: 16px;
	}
	.pdf-heading th, .pdf-heading td
	{
		font-size: 16px;
	}
	.performance p
	{
		padding-top: 8px;
	}
</style>
@endpush

@push('js')
  	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  	<script type="text/javascript">
	  	google.charts.load('current', {packages: ['corechart', 'line']});
		google.charts.setOnLoadCallback(drawAllCharts);

	    overallPerformanceDataURI = "";
		function drawOverallPerformance() 
		{
	      	var data = new google.visualization.DataTable();
	      	data.addColumn('number', '%');
	      	// data.addColumn('number', 'Approach Quality');
			data.addColumn('number', 'Understanding Others');
			data.addColumn('number', 'Adapting to Others');

			data.addRows([
				@foreach($per_approach_quality_arr as $index => $value)
					[{{ $loop->iteration }}, {{ $per_understanding_others_arr[$index] }}, {{ $per_adapting_to_others_arr[$index] }}],
				@endforeach
			]);

			var options = {
				hAxis: {
				  title: 'Overall Performance'
				},
				vAxis: {
				  // title: 'Popularity'
				  	ticks: [0, 20, 40, 80, 100],
				  	minValue: 0,
				    maxValue: 100,
				    format: '#\'%\''
				},
				series: {
				  1: {curveType: 'function'}
				}
			};

	      	var chart = new google.visualization.LineChart(document.getElementById('overall_performance_chart'));
	      	google.visualization.events.addListener(chart, 'ready', function () {
			    overallPerformanceDataURI 	= chart.getImageURI();
			    $('#overall_performance_img').html('<img src="'+ overallPerformanceDataURI +'"/>');
			});
	      	chart.draw(data, options);
	    }

	    approachQualityDataURI = "";
	    function drawApproachQuality() 
		{
	      	var data = new google.visualization.DataTable();
	      	data.addColumn('number', '%');
	      	data.addColumn('number', 'Decisions made after observing behaviors (%)');
			data.addColumn('number', 'Diagnosis made (%)');
			data.addColumn('number', 'Engagements made (%)');

			data.addRows([
				@foreach($per_adapting_to_others_arr as $index => $value)
					[{{ $loop->iteration }}, {{ $per_descisions_made_arr[$index] }}, {{ $per_potential_diagnosis_arr[$index] }}, {{ $per_potential_engagements_arr[$index] }}],
				@endforeach
			]);

			var options = {
				hAxis: {
				  title: 'Approach Quality'
				},
				vAxis: {
				  // title: 'Popularity'
				  	ticks: [0, 20, 40, 80, 100],
				  	minValue: 0,
				    maxValue: 100,
				    format: '#\'%\''
				},
				series: {
				  1: {curveType: 'function'}
				}
			};

	      	var chart = new google.visualization.LineChart(document.getElementById('approach_quality_chart'));
	      	google.visualization.events.addListener(chart, 'ready', function () {
			    approachQualityDataURI 	= chart.getImageURI();
			    $('#approach_quality_img').html('<img src="'+ approachQualityDataURI +'"/>');
			});
	      	chart.draw(data, options);
	    }

	    specificProfileDataURI = "";
	    function drawSpecificProfile() 
		{
	      	var data = new google.visualization.DataTable();
	      	data.addColumn('number', '%');
	      	data.addColumn('number', 'D');
			data.addColumn('number', 'I');
			data.addColumn('number', 'S');
			data.addColumn('number', 'C');

			data.addRows([
				@foreach($per_adapting_to_others_arr as $index => $value)
					[{{ $loop->iteration }}, {{ $per_descisions_made_arr[$index] }}, {{ $per_potential_diagnosis_arr[$index] }}, {{ $per_potential_engagements_arr[$index] }}, 20],
				@endforeach
			]);

			var options = {
				hAxis: {
				  title: 'Performance With Specific Profiles'
				},
				vAxis: {
				  // title: 'Popularity'
				  	ticks: [0, 20, 40, 80, 100],
				  	minValue: 0,
				    maxValue: 100,
				    format: '#\'%\''
				},
				series: {
				  1: {curveType: 'function'}
				}
			};

	      	var chart = new google.visualization.LineChart(document.getElementById('specific_profiles_chart'));
	      	google.visualization.events.addListener(chart, 'ready', function () {
			    specificProfileDataURI 	= chart.getImageURI();
			    $('#specific_profiles_img').html('<img src="'+ specificProfileDataURI +'"/>');
			});
	      	chart.draw(data, options);
	    }

	    function drawAllCharts()
	    {
	    	drawOverallPerformance();
	    	drawApproachQuality();
	    	drawSpecificProfile();
	    }
  	</script>
	<script type="text/javascript" src="{{ asset("js/html2canvas.min.js") }}"></script>
	<script type="text/javascript" src="{{ asset("js/jspdf.min.js") }}"></script>
	<script type="text/javascript">
		$('#download').click(function(){
			$('.top-download').hide();
			$('header').hide();
			$('.content-wrapper').css('background-image', 'unset');
			$('.pdf-logo').show();
			$('#overall_performance_chart').hide();
			$('#overall_performance_img').show();
			$('#overall_performance_stats tbody').hide();
			$('#approach_quality_chart').hide();
			$('#approach_quality_img').show();
			$('#approach_quality_stats tbody').hide();
			$('#specific_profiles_chart').hide();
			$('#specific_profiles_img').show();
			$('#specific_profiles_stats tbody').hide();
			$('#loader').show();
			html2canvas(document.querySelector("div.content-wrapper"), { useCORS: true, allowTaint: true }).then(canvas => {
			    $('.top-download').show();
				$('header').show();
				$('.content-wrapper').css('background-image', 'url("/img/bg.jpg")');
				$('.pdf-logo').hide();
				$('#overall_performance_chart').show();
				$('#overall_performance_img').hide();
				$('#overall_performance_stats tbody').show();
				$('#approach_quality_chart').show();
				$('#approach_quality_img').hide();
				$('#approach_quality_stats tbody').show();
				$('#specific_profiles_chart').show();
				$('#specific_profiles_img').hide();
				$('#specific_profiles_stats tbody').show();
			    // document.body.appendChild(canvas);
			    var imgData 	= canvas.toDataURL('image/jpeg'); 
			    // window.location = imgData;
				var pdf 		= new jsPDF('p', 'mm');
				pdf.setFontSize(40)
				// pdf.text(35, 25, 'Performance Analysis');
				pdf.addImage(imgData, 'JPEG', 10, 0, 190, 190);
				pdf.save('Comparative_Analysis-{{ date('Y-m-d H:i:s') }}.pdf');
				setTimeout(function(){
					$('#loader').hide();
				}, 2000);
				// console.log(imgData);     
			});
		});
		$(document).ready(function(){
			var today = new Date();
			$('#sys_date').text(today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear());
		});
	</script>
@endpush