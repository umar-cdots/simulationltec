@extends('adminlte::page')

@section('title', 'SIMMETRIC:GAIA')

@section('content')
<div class="content-header">
	<div class="container-fluid top-download" style="margin-bottom: 10px;">
		<button class="btn pull-right btn-download" id="download"><i class="fa fa-download"></i> Download</button>
		<div class="clearfix"></div>
	</div>
	<div class="container-fluid pdf-logo" style="margin-bottom: 10px; padding: 0; display: none;">
		<div class="col-md-12" style="padding: 0;"> 
			<div class="col-md-6">
				<img src="{{ asset('img/m-logos.png') }}" style="width: 40%;">
			</div>
			<div class="col-md-6 text-right">
				<h1 style="margin: 0px;">Performance Analysis</h1>
			</div>
			<div class="clearfix"></div>
			<table class="table table-bordered pdf-heading" style="margin-top: 10px; margin-bottom: 0;">
				<tbody>
					<tr>
						<th>Name</th>
						<td>{{ $game_play->user->name }}</td>
						<th>Email</th>
						<td>{{ $game_play->user->email }}</td>
						<th>Date</th>
						<td id="sys_date"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Overall performance</h3>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	{{-- <div class="box box-solid">
        		<div class="container">
	        		<p>
	        			All performance metrics are analyzed from the second turn of the simulation. This allows the user to familiarize with the interface and to make intentional choices based on the behaviors observed after the first turn.
	        		</p>
	        	</div>
        	</div>
        	<div class="clearfix"></div> --}}
          	{{-- <div class="col-md-6">
				<h4>Approach quality</h4>
			</div> --}}
			{{-- <div class="col-md-8">
				<p>
					How well have you played, making informed decisions based on observations, diagnosing leaders at every opportunity provided, and maximizing the number of engagements.
				</p>
			</div> --}}
			{{-- <div class="col-md-6">
				<div class="progress">
				    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $per_approach_quality }}%">{{ $per_approach_quality }}%</div>
				  </div>
	        </div>
			<div class="clearfix"></div> --}}
          	<div class="col-md-2">
				<h4>Understanding others</h4>
			</div>
			<div class="col-md-6">
				<p>
					How well have you identified the leaders' personal styles, selecting them with accuracy and avoiding mistakes.
				</p>
			</div>
			<div class="col-md-4">
				<div class="progress">
				    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $per_understanding_others }}%">{{ $per_understanding_others }}%</div>
				  </div>
	        </div>
			<div class="clearfix"></div>
          	<div class="col-md-2">
				<h4>Adapting to others</h4>
			</div>
			<div class="col-md-6">
				<p>
					How well have you flexed your style to the leaders' personal styles, understanding which engagements maximize their leadership effectiveness.
				</p>
			</div>
			<div class="col-md-4">
				<div class="progress">
				    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $per_adapting_to_others }}%">{{ $per_adapting_to_others }}%</div>
			    </div>
	        </div>
			<div class="clearfix"></div>
			{{-- <div class="box box-solid" style="border-top: 1px solid #d2d6de;">
        		<div class="container" style="margin-top: 10px;">
	        		<p>
	        			Your approach to address the challenge provided is inconsistent, lacking of method and structure. You have struggled to identify the personal profiles of your team members, and you haven't flexed your style effectively to maximize their leadership performance.
	        		</p>
	        	</div>
        	</div> --}}
        </div>
        <!-- /.box-body -->
  	</div>
  	<div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Understanding others</h3>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	<div class="box box-solid">
        		<div class="container">
	        		<p>
	        			@if($per_understanding_others < 50)
	        				The accuracy of your interpretations is low, demonstrating an insufficient understanding of the framework.
	        			@elseif($per_understanding_others >= 50 && $per_understanding_others < 75)
	        				The accuracy of your interpretations is modest, demonstrating a partial understanding of the framework.
	        			@elseif($per_understanding_others >= 75 && $per_understanding_others < 90)
	        				The accuracy of your interpretations is good, with some room for improvement to reach mastery level.
	        			@else
	        				The accuracy of your interpretations is outstanding, demonstrating a full understanding of the framework.
	        			@endif
	        		</p>
	        	</div>
        	</div>
        	<div class="clearfix"></div>
        	<div class="container-fluid">
        		<div class="col-md-12 margin-bottom">
	        		<div class="col-md-2 heading_center">
	        			<h4>Name</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Style</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 1</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 2</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 3</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 4</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 5</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 6</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 7</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 8</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Performance</h4>
	        		</div>
	        		<div class="clearfix"></div>
	        	</div>
        		@foreach($leaders as $leader)
	        		<div class="col-md-12 margin-bottom">
	        			<div class="col-md-2">
	        				<img class="img-circle pull-left" src="{{ asset($leader->logo) }}" alt="{{ $leader->name }}" style="width: 25%;">
	        				<h4 class="pull-left leader-name">{{ $leader->name }}</h4>
	        			</div>
	        			@php
	        				$diagnose 	= $leader->gameCharacter()->where('game_play_id', $game_play->id)->first()->personalityType;
	        			@endphp
	        			<div class="col-md-1 diagnose diagnose-{{ strtolower($diagnose->short_title) }}">
	        				<h4>{{ $diagnose->short_title }}</h4>
	        			</div>
	        			{{-- <div class="col-md-1"></div> --}}
	        			@php
	        				$correct_diagnose 	= 0;
	        				$diagnose_count		= 0;
	        			@endphp
	        			@foreach($game_data->where('leader_id', $leader->id)->all() as $data)
	        				@continue($data->leader_id != $leader->id)
	        				<div class="col-md-1 diagnose diagnose-{{ isset($data->personalityType->short_title) ? ($data->personalityType->short_title == $diagnose->short_title ? 'correct' : ($data->personalityType->short_title == $diagnose->oposite_personality_short_title ? 'opposite' : 'wrong')) : 'wrong' }}" title="{{ isset($data->personalityType->short_title) ? ($data->personalityType->short_title == $diagnose->short_title ? 'Correct' : ($data->personalityType->short_title == $diagnose->oposite_personality_short_title ? 'Opposite' : 'Wrong')) : 'N/A' }}">
	        					<h4>{{ $data->personalityType->short_title ?? 'N/A'}}</h4>
	        				</div>
	        				@isset($data->personalityType->short_title)
		        				@if($data->personalityType->short_title == $diagnose->short_title)
		        					@php
		        						$correct_diagnose++;
		        					@endphp
		        				@endif
	        				@endisset
	        				@php
	        					$diagnose_count++;
	        				@endphp
	        			@endforeach
	        			@if($diagnose_count < 7)
	        				@for(; $diagnose_count < 7; $diagnose_count++)
		        				<div class="col-md-1 diagnose diagnose-wrong" title="N/A">
	        						<h4>N/A</h4>
		        				</div>
	        				@endfor
	        			@endif
	        			<div class="col-md-1">
							<div class="progress">
							    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ round($correct_diagnose / 7 * 100) }}%">{{ round($correct_diagnose / 7 * 100) }}%</div>
						    </div>
				        </div>
	        		</div>
	        		<div class="clearfix"></div>
        		@endforeach
        	</div>
        </div>
    </div>
  	<div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Adapting to others</h3>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	<div class="box box-solid">
        		<div class="container">
	        		<p>
	        			@if($per_adapting_to_others < 50)
	        				The accuracy of your engagement style is low, proving limited understanding of how to adapt to people with different styles.
	        			@elseif($per_adapting_to_others >= 50 && $per_adapting_to_others < 75)
	        				The accuracy of your engagement style is modest, demonstrating a partial understanding about how to adapt to people with different styles.
	        			@elseif($per_adapting_to_others >= 75 && $per_adapting_to_others < 90)
	        				The accuracy of your engagement style is good, with some room for improvement to reach mastery level.
	        			@else
	        				The accuracy of your engagements is outstanding, demonstrating a full understanding of the best approaches for people with different styles.
	        			@endif
	        		</p>
	        	</div>
        	</div>
        	<div class="clearfix"></div>
        	<div class="container-fluid">
        		<div class="col-md-12 margin-bottom">
	        		<div class="col-md-2 heading_center">
	        			<h4>Name</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Style</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 1</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 2</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 3</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 4</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 5</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 6</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 7</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Quarter 8</h4>
	        		</div>
	        		<div class="col-md-1 heading_center">
	        			<h4>Performance</h4>
	        		</div>
	        		<div class="clearfix"></div>
	        	</div>
        		@foreach($leaders as $leader)
	        		<div class="col-md-12 margin-bottom">
	        			<div class="col-md-2">
	        				<img class="img-circle pull-left" src="{{ asset($leader->logo) }}" alt="{{ $leader->name }}" style="width: 25%;">
	        				<h4 class="pull-left leader-name">{{ $leader->name }}</h4>
	        			</div>
	        			@php
	        				$diagnose 	= $leader->gameCharacter()->where('game_play_id', $game_play->id)->first()->personalityType;
	        			@endphp
	        			<div class="col-md-1 diagnose diagnose-{{ strtolower($diagnose->short_title) }}">
	        				<h4>{{ $diagnose->short_title }}</h4>
	        			</div>
	        			{{-- <div class="col-md-1"></div> --}}
	        			@php
	        				$correct_engagements_count 	= 0;
	        				$diagnose_count		= 0;
	        			@endphp
	        			
	        			@foreach($stages as $stage)
	        				@php
		        				// $total_engagements 	= $leader->gameStagewiseEngagements()->where(['game_stage_id' => $stage])->count();
		        				$correct_engagements 	= $leader->gameStagewiseEngagements()->where(['game_stage_id' => $stage, 'leader_id' => $leader->id, 'accuracy' => 'correct'])->count();
		        				$correct_engagements_count += $correct_engagements;
		        			@endphp
	        				<div class="col-md-1 diagnose diagnose-correct" data-stage="{{ $stage }}" data-leader="{{ $leader->id }}">
	        					<h4>{{$correct_engagements}}/4</h4>
	        				</div>
	        			@endforeach
	        			@if($stages->count() < 7)
	        				@for($i = $stages->count(); $i < 7; $i++)
		        				<div class="col-md-1 diagnose diagnose-wrong" title="N/A">
	        						<h4>N/A</h4>
		        				</div>
	        				@endfor
	        			@endif
	        			<div class="col-md-1">
							<div class="progress">
							    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ round($correct_engagements_count / 28 * 100) }}%">{{ round($correct_engagements_count / 28 * 100) }}%</div>
						    </div>
				        </div>
	        		</div>
	        		<div class="clearfix"></div>
        		@endforeach
        	</div>
        </div>
    </div>
  	<div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Performance with specific styles</h3>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	<div class="box box-solid">
        		<div class="container-fluid" id="profile_summary">
        			
	        	</div>
        	</div>
        	<div class="clearfix"></div>
        	<div class="container-fluid">
        		<div class="col-md-12">
        			<div class="col-md-offset-1 col-md-5 heading_center">
        				<h4>Correct interpretation</h4>
        			</div>
        			<div class="col-md-offset-1 col-md-2 heading_center" style="padding: 0;">
        				<h4>Correct engagements</h4>
        			</div>
        			<div class="col-md-offset-1 col-md-2 heading_center">
        				<h4>Overall performance</h4>
        			</div>
	        		<div class="clearfix"></div>
        		</div>
        		<div class="col-md-12">
	        		<div class="col-md-offset-1 col-md-2 heading_center">
	        			<h4><abbr title="Sensitivity: How often did you identify the correct styles?">Sensitivity</abbr></h4>
	        		</div>
	        		<div class="col-md-offset-1 col-md-2 heading_center">
	        			<h4><abbr title="Specificity: How often did you avoid mistakes?">Specificity</abbr></h4>
	        		</div>
	        		<div class="clearfix"></div>
	        	</div>
	        	@php
	        		$profile_summary = array();
	        	@endphp
        		@foreach($personality_types as $personality_type)
        			@php
        				$divider 	= $specific_profiles[$personality_type->short_title]['true_positive'] + $specific_profiles[$personality_type->short_title]['false_negative'];
        				if($divider == 0)
        				{
        					$divider = 1;
        				}
        				$sensitivity = round($specific_profiles[$personality_type->short_title]['true_positive'] / $divider * 100);

        				$divider 	= $specific_profiles[$personality_type->short_title]['true_negative'] + $specific_profiles[$personality_type->short_title]['false_positive'];
        				if($divider == 0)
        				{
        					$divider = 1;
        				}
        				$specificty = round($specific_profiles[$personality_type->short_title]['true_negative'] / $divider * 100);
        				$correct_engagements = round($specific_profiles[$personality_type->short_title]['correct_engagements'] / $specific_profiles[$personality_type->short_title]['total_engagements'] * 100);
        				$performance = round(($sensitivity + $specificty + $correct_engagements) / 3);

        				$diff 		 = ($sensitivity + $specificty) / 2;
        				if($diff >= 66)
        				{
        					$profile_summary[$personality_type->short_title] = $first_part_of_summary['66'];
        				}
        				elseif($diff >= 33)
        				{
        					$profile_summary[$personality_type->short_title] = $first_part_of_summary['33'];
        				}
        				else
        				{
        					$profile_summary[$personality_type->short_title] = $first_part_of_summary['0'];
        				}

        				if(($diff > 66 && $correct_engagements < 66) || ($diff < 66 && $correct_engagements > 66))
        				{
        					$profile_summary[$personality_type->short_title] .= ' but ';
        				}
        				elseif(($diff >= 33 && $correct_engagements >= 33) || ($diff <= 33 && $correct_engagements <= 33))
        				{
        					$profile_summary[$personality_type->short_title] .= ' and ';
        				}
        				else
        				{
        					$profile_summary[$personality_type->short_title] .= ' and ';
        				}

        				if($correct_engagements >= 66)
        				{
        					$profile_summary[$personality_type->short_title] .= $second_part_of_summary['66'];
        				}
        				elseif($correct_engagements >= 33)
        				{
        					$profile_summary[$personality_type->short_title] .= $second_part_of_summary['33'];
        				}
        				else
        				{
        					$profile_summary[$personality_type->short_title] .= $second_part_of_summary['0'];
        				}

        				// if(($correct_engagements >= 66 && $diff >= 66) || ($correct_engagements >= 33 && $diff >= 33) || ($correct_engagements < 33 && $diff < 33))
        				// {
        				// 	$profile_summary[$personality_type->short_title] .= ' and ' . $second_part_of_summary['66'];
        				// 	// $profile_summary[$personality_type->short_title] .= ($diff >= 33 ? ' and ' : ' but ') . $second_part_of_summary['66'];
        				// }
        				// // elseif($correct_engagements >= 33)
        				// // {
        				// // 	$profile_summary[$personality_type->short_title] .= ($diff >= 33 ? ' and ' : ' but ') . $second_part_of_summary['33'];
        				// // }
        				// else
        				// {
        				// 	$profile_summary[$personality_type->short_title] .= ' but ' . $second_part_of_summary['0'];
        				// }
        			@endphp
	        		<div class="col-md-12 margin-bottom">
	        			<div class="col-md-1 diagnose diagnose-{{ strtolower($personality_type->short_title) }}">
	        				<h4>{{ $personality_type->short_title }}</h3>
	        			</div>
	        			<div class="col-md-2">
							<div class="progress">
							    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $sensitivity }}%">{{ $sensitivity }}%</div>
						    </div>
				        </div>
	        			<div class="col-md-offset-1 col-md-2">
							<div class="progress">
							    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $specificty }}%">{{ $specificty }}%</div>
						    </div>
				        </div>
	        			<div class="col-md-offset-1 col-md-2">
							<div class="progress">
							    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $correct_engagements }}%">{{ $correct_engagements }}%</div>
						    </div>
				        </div>
	        			<div class="col-md-offset-1 col-md-2">
							<div class="progress">
							    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $performance }}%">{{ $performance }}%</div>
						    </div>
				        </div>
	        			<div class="clearfix"></div>
	        		</div>
        		@endforeach
        	</div>
        </div>
    </div>
	{{-- <div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Approach analysis</h3>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          	<div class="col-md-4">
				<h4>Decisions made after observing behaviors</h4>
			</div>
			<div class="col-md-2">
				<h4>{{ $decisions_made_after_observing }}/{{ $total_decisions_made - $decisions_made_after_observing }}</h4>
			</div>
			<div class="col-md-6">
				<div class="progress">
				    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $per_descisions_made }}%">{{ $per_descisions_made }}%</div>
				  </div>
	        </div>
			<div class="clearfix"></div>
          	<div class="col-md-4">
				<h4>Number of interpretations</h4>
			</div>
			<div class="col-md-2">
				<h4>{{ $no_of_potential_diagnose }}/35</h4>
			</div>
			<div class="col-md-6">
				<div class="progress">
				    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $per_potential_diagnosis }}%">{{ $per_potential_diagnosis }}%</div>
				  </div>
	        </div>
			<div class="clearfix"></div>
          	<div class="col-md-4">
				<h4>Number of engagements</h4>
			</div>
			<div class="col-md-2">
				<h4>{{ $no_of_potential_engagements }}/140</h4>
			</div>
			<div class="col-md-6">
				<div class="progress">
				    <div class="progress-bar progress-bar-report" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{ $per_potential_engagements }}%">{{ $per_potential_engagements }}%</div>
			    </div>
	        </div>
			<div class="clearfix"></div>
        </div>
        <!-- /.box-body -->
  	</div> --}}
    <div class="box box-solid box-report">
        <div class="box-header with-border">
          <h3 class="box-title">Conclusion</h3>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        	<div class="box box-solid">
        		<div class="container">
	        		<p>
	        			@php
	        				$average = ($per_understanding_others + $per_adapting_to_others) / 2;
	        			@endphp
	        			@if($average + $average < 50)
	        				Considering your low performance in the simulation, it's strongly recommended to re-play it, leveraging the avaiable documentation related to the framework. Please put an effort to understand the style of your direct reports by observing their behaviors, and choosing the right interventions to maximize their effectiveness accordingly.
	        			@elseif($average >= 50 && $average < 75)
	        				With your performance you have demonstrated a basic understanding of the framework, and how to leverage it to maximize leadership effectiveness of your subordinates. Please re-play the simulation, and focus on observing and interpreting behaviors of your direct reports, flexing your style according to them.
	        			@elseif($average >= 75 && $average < 90)
	        				Despite your positive performance, it's recommended to re-play the simulation to raise your scores further and reach mastery level. Please pay attention to the details and stretch your understanding of the framework, analyzing behaviors to interpret personal styles, and engaging leaders with the appropriate interventions.
	        			@else
	        				Based on your performance, you demonstrated a comprehensive understanding of the framework, of how different people with specific profiles display behaviors, and how to flex your style according to it. Please consider this learning session completed.
	        			@endif
	        		</p>
	        	</div>
        	</div>
        	<div class="clearfix"></div>
        	<div class="container pdf-hide">
        		<div class="col-md-10" style="padding-left: 0px;">
        			<p>
        				Before playing the simulation again, please have a 15 minutes break. You can use this time to reflect on your performance and what you will do differently in the next attempt.
        			</p>
        		</div>
        		<div class="col-md-2" style="padding-right: 0px;">
    				<a href="{{ route('simulation') }}?action=play">
        				<button class="btn btn-success pull-right">Play Again</button>
        			</a>
        		</div>
        	</div>
        </div>
    </div>
</div>
<div id="dummy" style="display: none;">
	@foreach($personality_types as $personality_type)
		<div class="col-md-1 diagnose diagnose-{{ strtolower($personality_type->short_title) }} margin-bottom">
			<h4>{{ $personality_type->short_title }} </h3>
		</div>
		<div class="col-md-11">
    		<p>
    			{{ $profile_summary[$personality_type->short_title] }}
    		</p>
    	</div>
    	<div class="clearfix"></div>
	@endforeach
</div>
@endsection

@push('css')
<style type="text/css">
	.top-download
	{
		margin-bottom: 10px;
		padding-right: 0px;
	}
	.btn-download
	{
		background-color: #29ba74;
		border-color: #29ba74;
		color: #fff !important;
	}
	.btn-download:hover, .btn-download:active
	{
		color: #fff;
	}
	.box-report
	{
		border: 1px solid #9a9a9a !important;
	}
	.box-report .box-header
	{
		color: #fff !important;
		background-color: #9a9a9a !important;
	}
	.progress-bar-report
	{
		background: #29ba74 !important;
		font-size: 18px !important;
		line-height: 40px !important;
	}
	.progress
	{
		height: 40px !important;
	}
	.margin-bottom
	{
		margin-bottom: 10px;
	}
	.margin-bottom img
	{
		margin-right: 3px;
	}
	.margin-bottom h4
	{
		line-height: 20px;
	}
	h4.leader-name
	{
		font-size: 17px;
		max-width: 125px;
		white-space: nowrap;
	}
	.diagnose-d
	{
		background-color: #f57c00;
	}
	.diagnose-i
	{
		background-color: #afb42b; 
	}
	.diagnose-s
	{
		background-color: #ffc107;
	}
	.diagnose-c
	{
		background-color: #ffa000;
	}
	.diagnose-correct
	{
		background-color: #29ba74;
	}
	.diagnose-wrong
	{
		background-color: #d4df33;
	}
	.diagnose-opposite
	{
		background-color: #e71c57;
	}
	.diagnose h3, .diagnose h4
	{
		margin-top: 10px;
		text-align: center;
	}
	.diagnose
	{
		border-right: 5px solid #fff;
	}
	.heading_center
	{
		text-align: center;
	}
	.progress-bar
	{
		color: #000;
	}
	.content-wrapper
	{
		background-color: #FFF;
	}
	p
	{
		font-size: 16px;
	}
	#profile_summary p
	{
		padding-top: 8px;
	}
	.pdf-heading th, .pdf-heading td
	{
		font-size: 16px;
	}
	.heading_center
	{
		padding: 0px;
	}
</style>
@endpush

@push('js')
<script type="text/javascript" src="{{ asset("js/html2canvas.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("js/jspdf.min.js") }}"></script>
<script type="text/javascript">
	$('#download').click(function(){
		$('.top-download').hide();
		$('header').hide();
		$('.pdf-hide').hide();
		$('.content-wrapper').css('background-image', 'unset');
		$('.pdf-logo').show();
		$('#loader').show();
		html2canvas(document.querySelector("div.content-wrapper")).then(canvas => {
		    $('.top-download').show();
			$('header').show();
			$('.pdf-hide').show();
			$('.content-wrapper').css('background-image', 'url("/img/bg.jpg")');
			$('.pdf-logo').hide();
		    // document.body.appendChild(canvas);
		    var imgData 	= canvas.toDataURL('image/jpeg'); 
		    // window.location = imgData;
			var pdf 		= new jsPDF('p', 'mm');
			pdf.setFontSize(40)
			// pdf.text(35, 25, 'Performance Analysis');
			pdf.addImage(imgData, 'JPEG', 10, 0, 190, 298);
			pdf.save('Performance_Analysis-{{ $game_play->created_at }}.pdf');
			setTimeout(function(){
				$('#loader').hide();
			}, 2000);
			// console.log(imgData);     
		});
	});
	$(document).ready(function(){
		$('#profile_summary').html($('#dummy').html());
		var today = new Date();
		$('#sys_date').text(today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear());
	});
</script>
@endpush