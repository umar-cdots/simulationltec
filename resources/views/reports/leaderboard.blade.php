@extends('adminlte::page')

@section('title', 'SIMMETRIC:GAIA')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
			<h1 class="pull-left">Leaderboard</h1>
			<div class="clearfix"></div>
		</section>
		<br>
		<section class="content">
			<div class="box">
	            <div class="box-body">
	            	<table class="table table-bordered table-striped" id="table">
	            		<thead>
	            			<tr>
	            				<th>Sr#</th>
	            				<th>Project</th>
	            				<th>Session</th>
	            				<th>Name</th>
	            				<th>Score</th>
	            			</tr>
	            		</thead>
	            		<tbody>
	            			@foreach($users as $user)
		            			<tr>
		            				<td>{{ $loop->iteration }}</td>
		            				<td>{{ $user->session->project->title }}</td>
		            				<td>{{ $user->session->title }}</td>
		            				<td>{{ $user->name }}</td>
		            				<td>{{ $user->score }}%</td>
		            			</tr>
	            			@endforeach
	            		</tbody>
	            	</table>
	            </div>
	        </div>
		</section>
	</div>
@endsection

@push('js')
@endpush