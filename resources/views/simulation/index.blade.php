@extends('adminlte::page')

@section('title', 'SIMMETRIC:GAIA')

@section('content')
<!-- Content Header (Page header) -->
{{-- <section class="content-header">
  <h1>
    Time left
    <small><span id="timer">30</span> minutes</small> <i class="ion ion-android-stopwatch"></i>
  </h1>
</section>
 --}}
<div class="content-header">
 <div class="headHold">
  {{-- <div class="col-md-1 logo2">
    <img src="{{ asset('img/bcg_logo.png') }}" style="width: 100px;">
  </div> --}}
  <div class="col-md-12">
    <div class="info-box bg-green" style="background-color: #9a9a9a !important; margin-bottom: 0px;">
      <div class="col-md-2 playButton">
        <span class="info-box-icon">
          <a class="btn" style="color:white;font-size:20px; background-color: #6e6f73;" id="play" data-state="play" data-project-id="{{ $project->id }}" data-total-stages="{{ $tenure->total_stages }}">
            <i class="fa fa-play"></i> Play
          </a>
          <a href="#" class="btn" style="color:white;font-size:20px; display: none; background-color: #6e6f73;" id="next_turn">
            <i class="fa fa-forward"></i> Next
          </a>
        </span>
      </div>
      <div class="col-md-8 progressBar">
        <div class="progress" style="height: 5px; background-color: #6e6f73; margin-top: 15px;">
          <div class="progress-bar" id="progress_bar"></div>
        </div>
        <span class="progress-description">
          {{-- {{ $tenure->short_for_year }} <span data-year="1" id="year">1</span> --}} {{ $tenure->short_for_turn }} <span data-quarter="1" id="quarter">1</span>
        </span>
      </div>
      <div class="col-md-2 white_text">
        <span class="info-box-text">Current Turn</span>
        <span class="info-box-number" id="turn" data-turn="1">1 / {{ $tenure->years * $tenure->turns_per_year }}</span>
      </div>
     {{--  <span class="info-box-icon">
        <a class="btn" style="color:white;font-size:20px;" id="play" data-state="play" data-project-id="{{ $project->id }}" data-total-stages="{{ $tenure->total_stages }}">
          <i class="fa fa-play"></i> Play
        </a>
        <a href="#" class="btn" style="color:white;font-size:20px; display: none;" id="next_turn">
          <i class="fa fa-forward"></i> Next
        </a>
      </span>
      <div class="info-box-content">
        <span class="info-box-text">Current Turn</span>
        <span class="info-box-number" id="turn" data-turn="1">1 / {{ $tenure->years * $tenure->turns_per_year }}</span>
        <div class="progress" style="height: 5px;">
          <div class="progress-bar" id="progress_bar"></div>
        </div>
        <span class="progress-description">
          {{ $tenure->short_for_year }}<span data-year="1" id="year">1</span>{{ $tenure->short_for_turn }}<span data-quarter="1" id="quarter">1</span>
        </span>
      </div> --}}
      <!-- /.info-box-content -->
    </div>
  </div>
 </div>
  @push('js')
    <script type="text/javascript">
      var previous_progress = [];
    </script>
  @endpush
  <!-- Main content -->
  @foreach($steps as $step)
    <div class="content col-lg-2 stepsDiv" id="leader{{ $step->leader->id }}">
      <div class="box">
        <div class="box-header with-border" style="padding-bottom: 5px;">
          <i class="margin-r-5">
            <img src="{{ asset('img/' . $step->logo) }}" class="label-icon">
          </i><h3 class="box-title">{{ $step->title }}</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="box box-widget widget-user" style="margin-bottom: 30px;">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active bg-increasing" id="bg{{ $step->leader->id }}">
              <h3 class="widget-user-username">{{ $step->leader->name }}</h3>
              <h5 class="widget-user-desc">{{ $step->leader->designation }}</h5>
            </div>
            <div class="widget-user-image" style="top: 55%;">
              <img class="img-circle" src="{{ asset($step->leader->logo) }}" alt="{{ $step->leader->name }}">
            </div>
          </div>
          <div class="clearfix" style="margin-top: 35px;"></div>
          <strong><i><img src="{{ asset('img/quarterly.png') }}" class="label-icon" style="width: 12%;"></i> Quarterly Productivity</strong>
          <br>
          <div class="text-center">
            <div class="sparkline sparkline_chart{{ $step->leader->id }}" data-type="line" data-spot-Radius="3" data-highlight-Spot-Color="#f39c12" data-highlight-Line-Color="#222" data-min-Spot-Color="#f56954" data-max-Spot-Color="#00a65a" data-spot-Color="#39CCCC" data-offset="90" data-width="100%" data-height="50px" data-line-Width="2" data-line-Color="#39CCCC" data-fill-Color="rgba(57, 204, 204, 0.08)" data-tooltip-Format="@{{y:val}}%" data-value="{{ round($step->leader->initial_leadership_effectiveness * $step->quarterly_target * (!$loop->last ? 100 : 1), 2) }}">
              {{ round($step->leader->initial_leadership_effectiveness * $step->quarterly_target, 1) * (!$loop->last ? 100 : 1) }}
            </div>
            <div class="target-text quarterly_progress">{{ $step->quarterly_production_title }} = <span>{{ round($step->leader->initial_leadership_effectiveness * $step->quarterly_target * (!$loop->last ? 100 : 1), 1) }}{{ !$loop->last ? '%' : '' }}</span> </div>
            <div class="target-text quarterly_target">Target  = <span>{{ $step->quarterly_target * (!$loop->last ? 100 : 1) }}{{ !$loop->last ? '%' : '' }}</span></div>
          </div>
          @push('js')
            <script type="text/javascript">
              previous_progress[{{ $step->leader->id }}]  = {{ $step->leader->initial_leadership_effectiveness * $step->quarterly_target }};
            </script>
          @endpush
          <hr style="margin-top: 05px; margin-bottom: 10px;">
          <strong><i><img src="{{ asset('img/overall.png') }}" class="label-icon" style="width: 12%;"></i> Overall Progress</strong>
          <br>
          <div class="text-center overall-progress">
            <input type="text" class="knob knob_chart{{ $step->leader->id }}" value="{{ round($step->initial_productivity / $step->overall_target * 100, 1) }}" data-overall-target="{{ $step->overall_target }}" data-width="60" data-height="60" data-thickness="0.2" data-fgColor="#3c8dbc" data-readonly="true">
            <div class="knob-label overall_progress">{{ $step->overall_production_title }} = <span>{{ round($step->initial_productivity, 1) }}</span> </div>
            <div class="knob-label overall_target">Target = <span>{{ $step->overall_target }}</span></div>
          </div>
          <div class="manage-bg">
            <p>Manage</p>
          </div>
          {{-- <strong><i><img src="{{ asset('img/mangement.png') }}" class="label-icon" style="width: 12%;"></i> Management</strong> --}}
          <div class="row">
            <div class="col-lg-12 text-center">
              <a class="btn observe col-md-6" data-leader-id="{{ $step->leader->id }}">
                <input type="hidden" name="observed{{ $step->leader->id }}" class="observedInp" value="0">
                <i>
                  <img src="{{ asset('img/observe.png') }}" class="label-icon" style="width: 30%;">
                </i> Observe
              </a>
              <a class="btn diagnose col-md-6" data-leader-id="{{ $step->leader->id }}">
                <i>
                  <img src="{{ asset('img/diagnose.png') }}" class="label-icon" style="width: 30%;">
                </i> Interpret
              </a>
              <a class="btn engage col-md-6 col-md-offset-3" data-leader-id="{{ $step->leader->id }}">
                <i>
                  <img src="{{ asset('img/engage.png') }}" class="label-icon" style="width: 30%;">
                </i> Engage
              </a>
            </div>
          </div>  
        </div>
      </div>
      <!-- /.box -->
    </div>
    @push('modals')
      @foreach($engagementHeadings as $engagementHeading)
        <div class="modal modal-warning fade engagementModal" id="enagementModal{{ $step->leader->id }}_{{ $engagementHeading->quarter_no }}" engagements-count="0">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Engage</h4>
              </div>
              <div class="modal-body">
                <form class="engagementForm_{{ $engagementHeading->quarter_no }}" id="engagementAccord{{ $step->leader->id }}_{{ $engagementHeading->quarter_no }}" data-leader-id="{{ $step->leader->id }}" data-leader-name="{{ $step->leader->name }}">
                  {{-- <input type="hidden" name="leader_ids[]" value="{{ $step->leader->id }}"> --}}

                  {{-- @forelse ($engagementHeadings->chunk(1) as $chunk)
                    @foreach($chunk as $engagementHeading) --}}
                      <div class="col-md-12">
                        <div class="panel box box-primary engage_border">
                          <div class="box-header with-border">
                            <h4 class="box-title">
                              <a data-toggle="collapse" data-parent="#engagementAccord{{ $step->leader->id }}_{{ $engagementHeading->quarter_no }}" href="#collapse{{ $step->leader->id.$engagementHeading->id }}_{{ $engagementHeading->quarter_no }}" aria-expanded="true" class="engage_title">
                                {{ $engagementHeading->title }}
                              </a>
                            </h4>
                          </div>
                          <div id="collapse{{ $step->leader->id.$engagementHeading->id }}_{{ $engagementHeading->quarter_no }}" class="panel-collapse collapse in" aria-expanded="true">
                            <div class="box-body">
                              @foreach($engagementHeading->engagements->shuffle() as $engagement)
                                <label>
                                  <input type="checkbox" name="engagement[{{ $step->leader->id }}][]" class="engagement_checkbox" value="{{ $engagement->id }}">
                                  {{ $engagement->title }}
                                </label>
                                <br>
                              @endforeach
                            </div>
                          </div>
                      </div>
                    </div>
                    {{-- @endforeach
                  @empty
                    <p>No Engagments Available Yet.</p>
                  @endforelse --}}
                </form>
                <div class="clearfix"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      @endforeach
      <!-- /.modal -->
      <div class="modal modal-warning fade diagnoseModal" id="diagnoseModal{{ $step->leader->id }}">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Interpet</h4>
            </div>
            <div class="modal-body text-center">
              <form class="diagnoseForm" data-leader-id="{{ $step->leader->id }}" data-leader-name="{{ $step->leader->name }}"> 
                <h3>Select Personality Type</h3>
                <div class='button-wrapper'>
                  <input type="hidden" name="personality_type[{{ $step->leader->id }}]" class="personality_checkbox selected_personality" id="personality_type[{{ $step->leader->id }}]" value="-1" checked="">
                  @foreach($personalityTypes as $personality)
                    <input type="button" class="personality_checkbox btn{{ $loop->index + 1 }}" value="{{ $personality->short_title }}" onclick="document.getElementById('personality_type[{{ $step->leader->id }}]').value = {{ $personality->id }};">
                  @endforeach
                  {{-- <input type="button" name="" class="btn1" value="D">
                  <input type="button" name=""  class="btn2" value="C">
                  <input type="button" name="" class="btn3" value="I">
                  <input type="button" name="" class="btn4" value="S"> --}}
                </div>
              </form>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-warning pull-left reset_diagnose">Reset</button>
              <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

      <!-- /.modal -->
    @endpush
  @endforeach
  <input type="hidden" name="project_id" id="project_id" value="{{ $project->id }}">
  <input type="hidden" name="stage_no" id="stage_no" value="1">
  <input type="hidden" name="game_play_id" id="game_play_id" value="">
  <input type="hidden" name="game_stage_id" id="game_stage_id" value="">
  {{-- <div class="col-md-12 next-turn">
    <button class="btn btn-lg btn-success pull-right" id="next_turn">Next Turn <i class="fa fa-arrow-right"></i></button>
  </div> --}}
  <!-- /.content -->
  <div class="clearfix"></div>
</div>
@endsection

@push('modals')
  <div class="modal modal-warning fade" id="observeModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Observe</h4>
        </div>
        <div class="modal-body">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" id="diagnoseTab">
              {{-- <li class="active">
                <a href="#tab{{ $tenure->short_for_year }}1{{ $tenure->short_for_turn }}1" data-toggle="tab" aria-expanded="true">{{ $tenure->short_for_year }}1{{ $tenure->short_for_turn }}1</a>
              </li> --}}
            </ul>
            <div class="tab-content" id="diagnoseContent">
              {{-- <div class="tab-pane active" id="tab{{ $tenure->short_for_year }}1{{ $tenure->short_for_turn }}1">
                <p class="observation"></p>
              </div> --}}
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <div class="modal modal-warning fade" id="completionModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Simulation Completed</h4>
        </div>
        <div class="modal-body">
          <fieldset>
            <legend>Value chart outputs</legend>
            <div class="col-md-12 access_com">
              <div class="col-md-2">
                <img src="{{ asset('img/access.png') }}" alt="Access">
              </div>  
              <div class="col-md-6">
                <div class="progress">
                  <div class="progress-bar progress-bar-com" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%; height: 100px; ">0%</div>
                </div>
              </div>  
              <div class="col-md-4">
                <p>Total areas:</p>
                <p class="total_value"></p>
              </div>
            </div>
            <div class="col-md-12 planning_com">
              <div class="col-md-2">
                <img src="{{ asset('img/planning.png') }}" alt="Planning">
              </div>  
              <div class="col-md-6">
                <div class="progress">
                  <div class="progress-bar progress-bar-com" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%; height: 100px; ">0%</div>
                </div>
              </div>  
              <div class="col-md-4">
                <p>Total plans:</p>
                <p class="total_value"></p>
              </div>
            </div>
            <div class="col-md-12 contract_com">
              <div class="col-md-2">
                <img src="{{ asset('img/contract.png') }}" alt="Contract Approval">
              </div>  
              <div class="col-md-6">
                <div class="progress">
                  <div class="progress-bar progress-bar-com" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%; height: 100px; ">0%</div>
                </div>
              </div>  
              <div class="col-md-4">
                <p>Total contracts:</p>
                <p class="total_value"></p>
              </div>
            </div>
            <div class="col-md-12 installation_com">
              <div class="col-md-2">
                <img src="{{ asset('img/access.png') }}" alt="Installation">
              </div>  
              <div class="col-md-6">
                <div class="progress">
                  <div class="progress-bar progress-bar-com" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%; height: 100px; ">0%</div>
                </div>
              </div>  
              <div class="col-md-4">
                <p>Total arrays:</p>
                <p class="total_value"></p>
              </div>
            </div>
          </fieldset>
          <fieldset>
            <legend>Key output</legend>
            <div class="col-md-12 operation_com">
              <div class="col-md-2">
                <img src="{{ asset('img/operation.png') }}" alt="Operation">
              </div>  
              <div class="col-md-6">
                <div class="progress">
                  <div class="progress-bar progress-bar-com" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%; height: 100px; ">0%</div>
                </div>
              </div>  
              <div class="col-md-4">
                <p>Total energy(GWh):</p>
                <p class="total_value"></p>
              </div>
            </div>
          </fieldset>
          <fieldset style="margin-top: 10px;">
            <p class="completion_msg"></p>
          </fieldset>
          <div class="clearfix"></div>
          <div class="col-md-12 final_output text-center" style="margin-bottom: 10px;"></div>
          <div class="col-md-12 text-center" style="margin-bottom: 10px;">
            <a href="/performance_analysis" id="performance_analysis_href">
              <button class="btn btn-lg btn-success">
                Performance Analysis
              </button>
            </a>
            <div class="clearfix"></div>
          </div>
          {{-- <div class="col-md-12 text-center">
            <a href="{{ route('comparative_analysis') }}">
              <button class="btn btn-lg btn-warning">Comparative Analysis</button>
            </a>
            <div class="clearfix"></div>
          </div> --}}
          <div class="clearfix"></div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <div class="modal modal-warning fade" id="legendModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Legend</h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">  
            <div class="col-md-4">
              <div class="progress">
                <div class="progress-bar progress-bar-inc" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:100%;">Increasing</div>
              </div>
            </div>  
            <div class="col-md-8">
              <p>Green color indicates that your performance is being increased by every passing turn.</p>
            </div>
          </div>
          <div class="col-md-12">  
            <div class="col-md-4">
              <div class="progress">
                <div class="progress-bar progress-bar-stb" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:100%;">Stable</div>
              </div>
            </div>  
            <div class="col-md-8">
              <p>Yellow color indicates that your performance is stable.</p>
            </div>
          </div>
          <div class="col-md-12">  
            <div class="col-md-4">
              <div class="progress">
                <div class="progress-bar progress-bar-dec" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:100%;">Decreasing</div>
              </div>
            </div>  
            <div class="col-md-8">
              <p>Red color indicates that your performance is being decreased by every passing turn.</p>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <div id="tutorialSlider" class="modal fade" role="dialog">
    <div class="modal-dialog modalCustom">
      <!-- Modal content-->
      <div class="modal-content">
        {{-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tutorial</h4>
        </div> --}}
        <div class="modal-body">
          <div id="tutorial" class="carousel slide" data-ride="carousel" data-interval="false">
            <button type="button" class="close modal-close-btn" data-dismiss="modal">&times;</button>
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#tutorial" data-slide-to="0" class="active"></li>
              <li data-target="#tutorial" data-slide-to="1"></li>
              <li data-target="#tutorial" data-slide-to="2"></li>
              <li data-target="#tutorial" data-slide-to="3"></li>
              <li data-target="#tutorial" data-slide-to="4"></li>
              <li data-target="#tutorial" data-slide-to="5"></li>
              <li data-target="#tutorial" data-slide-to="6"></li>
              <li data-target="#tutorial" data-slide-to="7"></li>
              <li data-target="#tutorial" data-slide-to="8"></li>
              <li data-target="#tutorial" data-slide-to="9"></li>
              <li data-target="#tutorial" data-slide-to="10"></li>
              <li data-target="#tutorial" data-slide-to="11"></li>
              <li data-target="#tutorial" data-slide-to="12"></li>
              <li data-target="#tutorial" data-slide-to="13"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="{{ asset('img/tutorial/1.png') }}" style="width: 100%;" alt="Start Simulation">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/2.png') }}" style="width: 100%;" alt="Ultimate Goal">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/3.png') }}" style="width: 100%;" alt="Chained Performance">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/4.png') }}" style="width: 100%;" alt="SVPs">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/5.png') }}" style="width: 100%;" alt="Progress Colors Notation">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/6.png') }}" style="width: 100%;" alt="Manage">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/7.png') }}" style="width: 100%;" alt="Observe">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/8.png') }}" style="width: 100%;" alt="Read Observation">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/9.png') }}" style="width: 100%;" alt="Interpret">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/10.png') }}" style="width: 100%;" alt="Select Personality Type">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/11.png') }}" style="width: 100%;" alt="Engage">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/12.png') }}" style="width: 100%;" alt="Select Engagments">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/13.png') }}" style="width: 100%;" alt="Next Turn">
              </div>
              <div class="item">
                <img src="{{ asset('img/tutorial/14.png') }}" style="width: 100%;" alt="Countdown Time">
              </div>
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#tutorial" data-slide="prev">
              <i class="fa fa-angle-left"></i>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#tutorial" data-slide="next">
              <i class="fa fa-angle-right"></i>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal -->
  <div id="introSlider" class="modal fade" role="dialog">
    <div class="modal-dialog modalCustom">
      <!-- Modal content-->
      <div class="modal-content">
        {{-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Intro</h4>
        </div> --}}
        <div class="modal-body">
          <div id="intro" class="carousel slide" data-ride="carousel" data-interval="false">
            <button type="button" class="close modal-close-btn" data-dismiss="modal">&times;</button>
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#intro" data-slide-to="0" class="active"></li>
              <li data-target="#intro" data-slide-to="1"></li>
              <li data-target="#intro" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="{{ asset('img/intro/coal.jpg') }}" style="width: 100%;" alt="Coal Plant">
                <div class="carousel-caption">
                  <p>You have been staffed on a project in the Energy sector, working for WindPower Co. In your client's geography, a coal plant has served the area for over 40 years. With its 500MW capacity, it has provided electricity to more than 300,000 households. Pollution reports and frequent blackouts have convinced the local government to close the plant and transition to renewable energy.</p>
                </div>
              </div>
              <div class="item">
                <img src="{{ asset('img/intro/wind.jpg') }}" style="width: 100%;" alt="Wind Farm">
                <div class="carousel-caption">
                  <p>WindPower Co has built during the last years a huge wind farm, with more than 4 arrays and 100 onshore wind turbines. Despite its size, this is still not enough to replace the coal plant. BCG has been engaged by WindPower Co to transform the company, and your module is to manage the operations' leaders to accelerate the execution. The company will need to build more than 3 new arrays of wind turbines, generating a total of 7,000 GWh in two years.</p>
                </div>
              </div>
              <div class="item">
                <img src="{{ asset('img/intro/notes.jpg') }}" style="width: 100%;" alt="Notes">
                <div class="carousel-caption">
                  <p>During the first week you have managed to collect some information regarding the operations' SVPs that you are responsible to manage. Your objective is to understand their style, and maximize their leadership effectiveness by engaging them in the most appropriate way. Good luck!</p>
                </div>
              </div>
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#intro" data-slide="prev">
              <i class="fa fa-angle-left"></i>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#intro" data-slide="next">
              <i class="fa fa-angle-right"></i>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.modal -->
@endpush

@push('css')
  {{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}

	<style type="text/css">
    div.content-header
    {
      padding-top: 0px;
    }
    section.content
    {
      padding: 0px;
    }
    div.stepsDiv.content 
    {
      padding-top: 5px;
    }
    img.label-icon
    {
      width: 13%;
      margin-top: -6px;
    }
    .engage_title
    {
      color: #29ba74;
    }
    .engage_border
    {
      border-top-color: #29ba74 !important; 
    }
    #diagnoseTab li.active
    {
      border-top: unset !important;
    }
    #diagnoseTab li a
    {
      cursor: pointer;
    }
    input.personality_checkbox.selected
    {
      background-color: #256e5c;
    }
    .engagementForm .panel.box
    {
      min-height: 220px;
    }
    @media only screen and (min-width: 1200px)
    {
	    div.content.col-lg-2
	    {
	      width: 20% !important;
	    }
    }
    .widget-user-username
    {
      font-weight: bold !important;
      font-size: 22px !important;
    }
    .box-title
    /*{
      font-size: 17px !important;
    }
    .target-text
    {
      font-size: 13px !important;
    }*/
    .box
    {
      margin-bottom: 0px !important;
    }
    .next-turn
    {
      margin-bottom: 20px !important;
    }
    .next-turn button
    {
      width: 17.5% !important;
      background-color: #29ba74 !important;
      border-color: #29ba74 !important;
    }
    .info-box.bg-green span.info-box-icon
    {
      background-color: unset;
      line-height: 37px !important;
    }
    .info-box.bg-green div
    {
      background-color: #9a9a9a;
    }
    .info-box-content .progress
    {
      background-color: #6e6f73 !important;
    }
    .widget-user-header.bg-increasing
    {
      background-color: #29ba74 !important;
    }
    .widget-user-header.bg-decreasing
    {
      background-color: #e71c57 !important;
    }
    .widget-user-header.bg-stable
    {
      background-color: #d4df33 !important;
      /*color: #3e464c !important;*/
    }
    .button-wrapper{
      /*background-color: white;*/
      width: 270px;
      height: auto;
      margin: 0 auto;
      /*float: ;*/
      /*padding: 50px;*/
      /*position: relative;*/
      /*transform: rotate(-45deg);*/
    }

    .btn1:active, .btn2:active, .btn3:active, .btn4:active
    {
      background-color: #256e5c;
    }

    .btn2{
      /*background: #EFE3B3;*/
       background: #FFA000; 
      width: 125px;
      height: 125px;
      border-top-right-radius: 150px;
      outline: 0;
      font-size: 40px;
      border: 0;
      box-shadow: 1px 1px 10px #000;
      /*color:#000;*/
      text-align: center;
      margin:5px;
    }

    .btn1{
       text-align: center;
      background: #F57C00;
      width: 125px;
      height: 125px;
      border-top-left-radius: 150px;
      outline: 0;
      float: left;
        /*color:#000;*/
      font-size: 40px;
       border: 0;
       font-weight: 700;
         box-shadow: 1px 1px 10px #000;
         margin:5px;
    }

    .btn4{
      text-align: center;
      background: #FFC107;
      width: 125px;
      height: 125px;
      border-bottom-left-radius: 150px;
      left: 50%;
        /*color:#000;*/
      outline: 0;
      margin:5px;
      border: 0;
      font-size: 40px;
      font-weight: 700;
      box-shadow: 1px 1px 10px #000;
    }

    .btn3{
      text-align: center;
      background: #AFB42B;
      width: 125px;
      height: 125px;
      border-bottom-right-radius: 150px;
      outline: 0;
        /*color:#000;*/
      float: right;
      font-size: 40px;
      border: 0;
      font-weight: 700;
      box-shadow: 1px 1px 10px #000;
      margin:5px;
    }
    .bg-yellow-active, .modal-warning .modal-header, .modal-warning .modal-footer
    {
      background: #fff !important;
      color: #000 !important;
      border-color: #e5e5e5;
    }
    .bg-yellow, .callout.callout-warning, .alert-warning, .label-warning, .modal-warning .modal-body {
      background-color: #fff !important;
      color: #000 !important;
    }
    .box-body
    {
      color:#000;
    }
    .box-body label
    {
      font-size: 13px;
    }
    .nav-tabs-custom,.nav-tabs-custom>.tab-content
    {
      background: none;
    }
    .nav-tabs-custom>.nav-tabs>li>a
    {
      background: #ecf0f5;
      color:#000;
    }
    .nav-tabs-custom>.nav-tabs>li.active 
    {
      border-top-color: #fff;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a
    {
      background: #6e6f73;
      color:#fff;
    }
    #completionModal fieldset
    {
      padding: .35em .625em .75em;
      margin: 0 2px;
      border: 1px solid silver;
    }
    #completionModal legend
    {
      width: auto;
      margin-bottom: 0px;
      font-size: 14px;
      padding: 0px 2px;
      border: unset;
    }
    #completionModal .progress-bar-com
    {
      background: #29ba74 !important;
      font-size: 20px !important;
      line-height: 40px !important;
      color: #000;
    }
    #completionModal .progress
    {
      height: 40px !important;
    }
    .info-box-icon 
    {
      height: auto;
      font-size: 45px;
      line-height: 40px;
      border-radius: 5px;
    }
    .info-box.bg-green div
    {
      background: none;
    }
    .info-box 
    {
      display: block;
      min-height: 44px;
    }
    .info-box-icon
    {
      width: 100%;
    }
    .progress
    {
      margin-bottom: 0;
      margin-top: 15px;
    }
    .progress-description 
    {
      margin: 0;
      margin-top: 5px;
      color: #fff;
    }
    .white_text
    {
      color: #fff;
    }
    #legendModal .progress
    {
      height: 40px !important;
      margin-top: 0px !important;
    }
    #legendModal .progress-bar
    {
      font-size: 20px !important;
      line-height: 40px !important;
    }
    #legendModal .progress-bar-inc
    {
      background: #29ba74 !important;
    }
    #legendModal .progress-bar-stb
    {
      background: #d4df33 !important;
    }
    #legendModal .progress-bar-dec
    {
      background: #e71c57 !important;
    }
    .modalCustom
    {
      max-width: 70% !important;
      width: 100% !important;
    }
    .carousel-caption {
      /*right: 0 !important;*/
      left: 20px !important;
      width: 96% !important;
      text-align: left !important;
      padding-bottom: 20px;
      bottom: 10% !important;
      background-color: rgba(0, 0, 0, 0.6);
    }
    .carousel-caption h3
    {
      font-size: 42px !important;
    }
    .carousel-caption p
    {
      font-size: 18px !important;
      padding: 0 10px;
    }
    .modal-close-btn
    {
      position: absolute;
      color: white;
      z-index: 99999999;
      right: -43px;
      top: -32px;
      opacity: 0.9;
      font-size: 50px;
    }
    .modal-close-btn:hover
    {
      opacity: 0.7;
      color: white;
    }
    div.manage-bg p
    {
      text-align: center;
      background-color: #29ba74;
      color: #FFF;
    }
		
		@media screen and (max-width:1151px)
		{
			.headHold{
    		width: 100%;
 	   		height: 60px;
			}
			.logo2 img
		{
			width: 100% !important;
		}
		}
		@media screen and (max-width:961px)
		{
			.progress-description {
				width: 100%;
				text-align: center;
			}
			.logo2 ,.white_text
			{
				text-align: center;
			}
			.logo2 img
		{
			width: 15% !important;
		}
			.headHold {
    width: 100%;
    height: auto;
				margin: 10px 0;
}
			.logo2
			{
				margin-bottom: 10px;
			}
		}
		@media screen and (max-width:767px)
		{
			.navbar-header {
    float: left;
    width: 35%;
    margin: 0;
    height: auto;
    padding: 0;
}
			.navbar-header .navbar-brand {
    margin: 0px !important;
    padding: 0px !important;
    width: 32%;
}
			.navbar-toggle {
    color: #29ba74;
    border: 0;
    margin: 0;
    padding: 0;
    float: right;
    margin-top: 5px;
    position: relative;
    left: 10px;
}
			.navbar-header {
    float: left;
    width: 100% !important;
				padding: 10px 5px;
				box-sizing: border-box;
}
			.navbar-collapse.pull-left {
    float: none !important;
    width: 100%;
}
		}
		@media screen and (max-width:420px)
		{
			.progressBar
			{
				width: 100%;
			}
			.progressBar .progress
			{
				width: 100%;
				margin: 0;
				margin-top: 15px;
			}
			.playButton
			{
				height: 40px;
			}
			.navbar-collapse.pull-left {
    float: none !important;
    width: 100%;
    margin: 0;
}
		}
</style>
@endpush
@push('js')
	<!-- Sparkline -->
	<script src="{{ asset('vendor/adminlte/plugins/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{ asset('vendor/adminlte/plugins/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    {{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
	<script>
    $('.help-dropdown').show();
    dirtyEngagementFields = false;
    $('.engagementModal input[type="checkbox"]').change(function(){
      dirtyEngagementFields = true;
    });
	  $(".sparkline").each(function () {
	    var $this = $(this);
	    $this.sparkline('html', $this.data());
	  });
	  $(".knob").knob({
	    'format': function (label) {
	        return label + " %";
	      }
	  });
    $('input.personality_checkbox').click(function(){
      $(this).parents('.diagnoseForm').find('input.personality_checkbox').removeClass('selected');
      $(this).addClass('selected');
    });
	  var minutes_left = 30;
    interval         = 0;
	  $("#play").click(function(e){
	    e.preventDefault();
      var node  = $(this);
      var state = $("#play").attr('data-state');
      if(state == "playing")
      {
        toastr['error']("Simulation is Already Playing.")
      }
      else if(state == "ends")
      {
        location.reload();
      }
      else
      {
        var project_id = $(node).data('project-id');
        $.ajax({
          type: "POST",
          url: "{{ route('ajax.start_game') }}",
          data: "project_id=" + project_id,
          dataType: "json",
          cache: false,
          success: function(response){
            if(response.status == "success")
            {
              $('#loadIntro').trigger('click');
              window.onbeforeunload = function(e) {
                return 'Simulation is Running. Are You Sure You Want to Leave This Page.';
              };
              toastr['success'](response.message);
              $(node).text("Playing");
              $(node).attr('data-state', "playing");
              // $('i.pa_anchor').parent().attr('href', "performance_analysis/" + response.data.game_play_id);
              $('#game_play_id').val(response.data.game_play_id);
              $('#game_stage_id').val(response.data.game_stage_id);
              $('#stage_no').val(response.data.stage_no);
              $('.navbar-nav').find('li').hide();
              $('.navbar-nav').find('li:first-child').show();
              $('.help-menu').find('li').show();
              $('.navbar-nav').find('li.sub-legend').show();
              $('ul.navbar-nav').first().append('<li id="timerli"><a href="#" onclick="return false;">Time Left <small><span id="timer">30</span> Minutes</small> <i class="ion ion-android-stopwatch"></i></a></li>');
              $(node).hide();
              $("#next_turn").show();
              loadObservations();
              interval = setInterval(function(){
                minutes_left--;
                if(minutes_left < 0)
                {
                  $('.navbar-nav').find('li').show();
                  toastr['info']("Times UP!");
                  endGame("timesup");
                  // clearInterval(interval);
                }
                else
                {
                  $('#timer').text(minutes_left);
                }
              }, 60000);
            }
            else
            {
              toastr['error'](response.message);
            }
          },
          error: function(response){
            toastr['error']("Something Went Wrong.");
          }
        });
      }
	  });
    $(document).ajaxStart(function(){
      $("#next_turn").addClass('waiting-ajax-response');
    });
    $(document).ajaxComplete(function(){
      $("#next_turn").removeClass('waiting-ajax-response');
    });
	  var progress_bar        = 0;
    var progress_per_turn   = 100 / {{ $tenure->years * $tenure->turns_per_year }};
	  $("#next_turn").click(function(){
      if($(this).hasClass('waiting-ajax-response'))
        return;
	    var state = $("#play").attr('data-state');
	    if(state == "playing")
	    {
        var project_id    = $('#project_id').val();
        var game_play_id  = $('#game_play_id').val();
        var game_stage_id = $('#game_stage_id').val();
        var stage_no      = parseInt($('#stage_no').val());
	      var turn          = parseInt($("#turn").attr('data-turn'));
	      if(turn <= {{ $tenure->total_stages }})
	      {
          turn++;
	        var year     = parseInt($("#year").attr('data-year'));
	        var quarter  = parseInt($("#quarter").attr('data-quarter'));
	        if(quarter == {{ $tenure->turns_per_year }})
	          year++;
	        // quarter   = quarter == {{ $tenure->turns_per_year }} ? 1 : ++quarter;
          quarter++;
	        
          isError             = false;
          request             = "";
          diagnoseFormNodes   = $('.diagnoseForm');
          engagementFormNodes = $('.engagementForm_' + (turn - 1));

          $(diagnoseFormNodes).each(function(i, node){
            var leader        = $(node).data('leader-name');
            request          += "&" + $(node).serialize();
          });

          $(engagementFormNodes).each(function(i, node){
            var leader        = $(node).data('leader-name');
            var checked_count = $(node).find('input:checked').length;

            if(checked_count == 0)
            {
              isError = true;
              toastr['error']("Please select one engagement for " + leader);
            }
            else if(checked_count > 1)
            {
              isError = true;
              toastr['error'](leader + " Cannot Have More Than 1 Engagements.");
            }
            // else if(checked_count > 4)
            // {
            //   isError = true;
            //   toastr['error'](leader + " Cannot Have More Than 4 Engagements.");
            // }
            else
            {
              request += "&" + $(node).serialize();
            }
          });
          if(!isError)
          {
            if(dirtyEngagementFields)
            {
              request += "&stage_no=" + stage_no + "&project_id=" + project_id + "&game_play_id=" + game_play_id + "&game_stage_id=" + game_stage_id;
              $.ajax({
                type: "POST",
                url: "{{ route('ajax.next_stage') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function(response){
                  if(response.status == "success")
                  {
                    toastr['success'](response.message);
                    progress_bar     += progress_per_turn;
                    dirtyEngagementFields = false;
                    $('.observedInp').val(0);
                    $('#stage_no').val(stage_no + 1);
                    $("#turn").attr('data-turn', turn);
                    $("#year").attr('data-year', year);
                    $("#quarter").attr('data-quarter', quarter);
                    $("#turn").text(turn + "/{{ $tenure->total_stages }}");
                    $('#progress_bar').css('width', progress_bar + '%');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    var stepsDivs = $('.stepsDiv');
                    $(response.data.progress).each(function(i, obj){
                      var node = $('div#leader' + obj.leader_id);
                      var is_last = i+1 == response.data.progress.length;
                      // console.log('i=>' + i);
                      // console.log('is_last=>' + is_last);
                      // console.log('length=>' + response.data.progress.length);
                      var total_productivity = stage_no == 1 ? obj.productivity : parseFloat($(node).find('.overall_progress span').text()) + obj.productivity;
                      $(node).find('.quarterly_progress span').text((obj.initial_value * (!is_last ? 100 : 1)).toFixed(1) + (!is_last ? '%' : ''));
                      $(node).find('.overall_progress span').text((Math.round(total_productivity * 100) / 100).toFixed(1));
                      var sparkline_value = $(node).find('.sparkline').data('value');
                      $(node).find('.sparkline').data('value',  sparkline_value + ',' + (obj.initial_value * (!is_last ? 100 : 1)));
                      $(node).find('.sparkline').text($(node).find('.sparkline').data('value'));
                      var overall_divsible = parseFloat($(node).find('.knob').data('overall-target'));
                      var knobNode         = $(node).find('.knob');
                      var percentage       = Math.round((total_productivity / overall_divsible) * 100);
                      $(knobNode).val(percentage > 100 ? 100 : percentage);
                      $(node).find('.knob').parent().html(knobNode);
                      if(obj.initial_value == previous_progress[obj.leader_id])
                      {
                        $('#bg' + obj.leader_id).removeClass('bg-decreasing bg-increasing');
                        $('#bg' + obj.leader_id).addClass('bg-stable');
                        console.log(obj.leader_id + ' # stable');
                      }
                      else if(obj.initial_value > previous_progress[obj.leader_id])
                      {
                        $('#bg' + obj.leader_id).removeClass('bg-decreasing bg-stable');
                        $('#bg' + obj.leader_id).addClass('bg-increasing');
                        console.log(obj.leader_id + ' # increasing');
                      }
                      else
                      {
                        $('#bg' + obj.leader_id).removeClass('bg-increasing bg-stable');
                        $('#bg' + obj.leader_id).addClass('bg-decreasing');
                        console.log(obj.leader_id + ' # decreasing');
                      }
                      previous_progress[obj.leader_id] = obj.initial_value;
                    });
                    onCompleteLoad();
                    if(turn == {{ $tenure->total_stages }} + 1)
                    {
                      turn--;
                      toastr['success']("Simulation Completed.");
                      $("#turn").attr('data-turn', turn);
                      $("#turn").text(turn + "/{{ $tenure->total_stages }}");
                      $('#stage_no').val(stage_no - 1);
                      $('#performance_analysis_href').attr('href', 'performance_analysis/' + game_play_id);
                      $('#completionModal').modal('show');
                      endGame("complete");
                    }
                    else
                    {
                      $('#game_stage_id').val(response.data.game_stage_id);
                      $("#year").text(year);
                      $("#quarter").text(quarter);
                      loadObservations();
                    }
                    $(".sparkline").each(function () {
                      var $this = $(this);
                      $this.sparkline('html', $this.data());
                    });
                    $(".knob").knob({
                      format: function (label) {
                          return label + " %";
                        }
                    });
                  }
                  else
                  {
                    toastr['error'](response.message);
                  }
                },
                error: function(response){
                  toastr['error']("Something Went Wrong.");
                }
              });
            }
            else
            {
              if(confirm("You didn't make any decision during this turn. Are you sure that you want to proceed to the next turn?"))
              {
                dirtyEngagementFields = true;
                $("#next_turn").trigger("click");
              }
            }
          }
	      }
	      else
	      {
	        progress_bar = 100;
          $('#progress_bar').css('width', progress_bar + '%');
          $("html, body").animate({ scrollTop: 0 }, "slow");
	        toastr['success']("Simulation Completed.");
	      }
	    }
	    else
	    {
	      toastr['error']("You must Play Simulation First.")
	    }
	  });
    function endGame(type)
    {
      var node          = $("#play");
      var project_id    = $(node).data('project-id');
      var game_play_id  = $('#game_play_id').val();
      $.ajax({
        type: "POST",
        url: "{{ route('ajax.end_game') }}",
        data: "project_id=" + project_id + "&game_play_id=" + game_play_id + "&type=" + type,
        dataType: "json",
        cache: false,
        success: function(response){
          if(response.status == "success")
          {
            toastr['success'](response.message);
            $(node).text("Stopped");
            $(node).attr('data-state', "ends");
            $('.navbar-nav').find('li').show();
            $('.next-turn').remove();
            $('#timerli').hide();
            $(node).show();
            $('#next_turn').hide();
            clearInterval(interval);
            window.onbeforeunload = function(e) {};
          }
          else
          {
            toastr['error'](response.message);
          }
        },
        error: function(response){
          toastr['error']("Something Went Wrong.");
        }
      });
    }
    observations = [];
    observered   = [];
    function loadObservations()
    {
      var game_play_id  = $('#game_play_id').val();
      var game_stage_id = $('#game_stage_id').val();
      var stage_no      = $('#stage_no').val();
      var idd           = $('.progress-description').text().trim();
      $('.observe').each(function(i, node){
        var leader_id     = $(node).data('leader-id');
        if(typeof observations[leader_id] == 'undefined')
        {
          observations[leader_id] = [];
        }
        if(typeof observered[leader_id + ':' + stage_no] == 'undefined')
        {
          $.ajax({
            type: "GET",
            url: "{{ route('ajax.observation') }}",
            data: "leader_id=" + leader_id + "&game_play_id=" + game_play_id + "&game_stage_id=" + game_stage_id + "&stage_no=" + stage_no,
            dataType: "json",
            cache: false,
            success: function(response){
              if(response.status == "success")
              {
                observered[leader_id + ':' + stage_no]  = true;
                observations[leader_id].push({stage_no: stage_no, observation: response.data.observation, id: idd});
              }
              else
              {
                toastr['error'](response.message);
                $("html, body").animate({ scrollTop: 0 }, "slow");              
              }
            },
            error: function(response){
              toastr['error']("Something Went Wrong.");
            }
          });
        }
      });
    }
    $('.observe').click(function(){
      var state = $("#play").attr('data-state');
      if(state == "playing")
      {
        var node          = $(this);
        var leader_id     = $(node).data('leader-id');
        var game_play_id  = $('#game_play_id').val();
        var game_stage_id = $('#game_stage_id').val();
        var stage_no      = $('#stage_no').val();
        var idd           = $('.progress-description').text().trim();
        if(typeof observations[leader_id] == 'undefined')
        {
          observations[leader_id] = [];
        }
        if(typeof observered[leader_id + ':' + stage_no] == 'undefined')
        {
          $.ajax({
            type: "GET",
            url: "{{ route('ajax.observation') }}",
            data: "leader_id=" + leader_id + "&game_play_id=" + game_play_id + "&game_stage_id=" + game_stage_id + "&stage_no=" + stage_no,
            dataType: "json",
            cache: false,
            success: function(response){
              if(response.status == "success")
              {
                observered[leader_id + ':' + stage_no]  = true;
                observations[leader_id].push({stage_no: stage_no, observation: response.data.observation, id: idd});
                console.log(observations[leader_id]);
                $('#diagnoseTab').html("");
                $('#diagnoseContent').html("");
                $(observations[leader_id]).each(function(leader, instance){
                  console.log(instance);
                  if(typeof instance.observation == 'undefined')
                    return;
                  $('#diagnoseTab').append('<li><a href="#tab'+ instance.id +'" data-toggle="tab" aria-expanded="true">'+ instance.id +'</a></li>');
                  $('#diagnoseContent').append('<div class="tab-pane" id="tab'+ instance.id +'"><p class="observation">'+ instance.observation +'</p></div>');
                });
                $('#diagnoseTab').tab();
                $('#diagnoseTab li:last-child').addClass('active');
                $('#diagnoseContent div:last-child').addClass('active');
                // $('a[href="#tab'+ idd +'"]').trigger('click');
                $('#observeModal').modal('show');
                $('input[name="observed'+ leader_id +'"]').val(1);
                $.ajax({
                  type: "POST",
                  url: "{{ route('ajax.observed') }}",
                  data: "leader_id=" + leader_id + "&game_play_id=" + game_play_id + "&game_stage_id=" + game_stage_id,
                  dataType: "json",
                  cache: false,
                  success: function(response)
                  {
                  },
                  error: function()
                  {
                  }
                });
              }
              else
              {
                toastr['error'](response.message);
                $("html, body").animate({ scrollTop: 0 }, "slow");              
              }
            },
            error: function(response){
              toastr['error']("Something Went Wrong.");
            }
          });
        }
        else
        {
          $('#diagnoseTab').html("");
          $('#diagnoseContent').html("");
          // console.log(observations[leader_id]);
          $(observations[leader_id]).each(function(leader, instance){
            if(typeof instance == 'undefined')
              return;
            // console.log('i=>' + leader);
            // console.log('s=>' + instance.observation);
            $('#diagnoseTab').append('<li><a href="#tab'+ instance.id +'" data-toggle="tab" aria-expanded="true">'+ instance.id +'</a></li>');
            $('#diagnoseContent').append('<div class="tab-pane" id="tab'+ instance.id +'"><p class="observation">'+ instance.observation +'</p></div>');
          });
          $('#diagnoseTab').tab();
          $('#diagnoseTab li:last-child').addClass('active');
          $('#diagnoseContent div:last-child').addClass('active');
          $('#observeModal').modal('show');
          $('input[name="observed'+ leader_id +'"]').val(1);
          $.ajax({
            type: "POST",
            url: "{{ route('ajax.observed') }}",
            data: "leader_id=" + leader_id + "&game_play_id=" + game_play_id + "&game_stage_id=" + game_stage_id,
            dataType: "json",
            cache: false,
            success: function(response)
            {
            },
            error: function()
            {
            }
          });
        }
      }
      else
      {
        toastr['error']("You must Play Simulation First.")
      }
    }); 
    $('.engage').click(function(){
      var state = $("#play").attr('data-state');
      if(state == "playing")
      {
        var node          = $(this);
        var leader_id     = $(node).data('leader-id');
        var game_play_id  = $('#game_play_id').val();
        var game_stage_id = $('#game_stage_id').val();
        var turn          = parseInt($("#turn").attr('data-turn'))
        $('#enagementModal' + leader_id + '_' + turn).modal('show');
        // var isObserved    = $('input[name="observed'+ leader_id +'"]').val();
        // if(isObserved == 1)
          engageAfterObservation(leader_id, game_play_id, game_stage_id);
      }
      else
      {
        toastr['error']("You must Play Simulation First.")
      }
    });
    $('.diagnose').click(function(){
      var state = $("#play").attr('data-state');
      if(state == "playing")
      {
        var node          = $(this);
        var leader_id     = $(node).data('leader-id');
        $('#diagnoseModal' + leader_id).modal('show');
      }
      else
      {
        toastr['error']("You must Play Simulation First.")
      }
    });
    $('.engagementModal').on('click', 'input.engagement_checkbox', function(){
      var node  = $(this);
      var count = parseInt($(node).parents('.engagementModal').attr('engagements-count'));
      if($(node).is(':checked'))
      {
        if(count == 4)
        {
          toastr['error']("Maximum 4 Engagements Allowed.");
          $(node).prop('checked', false);
        }
        else
        {
          count += 1;
        }
      }
      else
      {
        count -= 1;
      }
      setTimeout(function(){
        $(node).parents('.engagementModal').attr('engagements-count', count);
      });
    });
    $('.reset_diagnose').click(function(){
      var node = $(this).parent().prev().find('form');
      $(node).find('input.selected_personality').val(-1);
      $(node).find('input.personality_checkbox').removeClass('selected');
    });
    function engageAfterObservation(leader_id, game_play_id, game_stage_id)
    {
      $.ajax({
        type: "POST",
        url: "{{ route('ajax.engaged_after_observing') }}",
        data: "leader_id=" + leader_id + "&game_play_id=" + game_play_id + "&game_stage_id=" + game_stage_id,
        dataType: "json",
        cache: false,
        success: function(response)
        {
        },
        error: function()
        {
        }
      });
    }
    function onCompleteLoad()
    {
      // $('#completionModal').find('.final_output').html("<h3 style='margin-top:0px;'>Result</h3>" + $('.overall-progress').last().html());
      // var node = $('#completionModal').find('.final_output');
      // var total_productivity = parseFloat($(node).find('.overall_progress span').text());
      // var overall_divsible = parseFloat($(node).find('.knob').data('overall-target'));
      // var knobNode         = $(node).find('.knob');
      // var percentage       = Math.round((total_productivity / overall_divsible) * 100);
      // $(knobNode).val(percentage > 100 ? 100 : percentage);
      // $(node).find('.knob').parent().html(knobNode);
      // $('#completionModal').find('.final_output').find(".knob").knob({
      //   format: function (label) {
      //       return label + " %";
      //     }
      // });
      var access_value        = parseFloat($('div.stepsDiv:nth-child(2)').find('input.knob').val());
      var access_progress     = parseFloat($('div.stepsDiv:nth-child(2)').find('.overall_progress span').text());
      var access_target       = parseFloat($('div.stepsDiv:nth-child(2)').find('.overall_target span').text());

      var planning_value      = parseFloat($('div.stepsDiv:nth-child(3)').find('input.knob').val());
      var planning_progress   = parseFloat($('div.stepsDiv:nth-child(3)').find('.overall_progress span').text());
      var planning_target     = parseFloat($('div.stepsDiv:nth-child(3)').find('.overall_target span').text());

      var contract_value      = parseFloat($('div.stepsDiv:nth-child(4)').find('input.knob').val());
      var contract_progress   = parseFloat($('div.stepsDiv:nth-child(4)').find('.overall_progress span').text());
      var contract_target     = parseFloat($('div.stepsDiv:nth-child(4)').find('.overall_target span').text());

      var installation_value  = parseFloat($('div.stepsDiv:nth-child(5)').find('input.knob').val());
      var installation_progress     = parseFloat($('div.stepsDiv:nth-child(5)').find('.overall_progress span').text());
      var installation_target = parseFloat($('div.stepsDiv:nth-child(5)').find('.overall_target span').text());

      var operation_value     = parseFloat($('div.stepsDiv:nth-child(6)').find('input.knob').val());
      var operation_progress  = parseFloat($('div.stepsDiv:nth-child(6)').find('.overall_progress span').text());
      var operation_target    = parseFloat($('div.stepsDiv:nth-child(6)').find('.overall_target span').text());

      var node                = $('#completionModal').find('.access_com');
      $(node).find('.progress-bar').attr('aria-valuenow', access_value);
      $(node).find('.progress-bar').css('width', access_value + '%');
      $(node).find('.progress-bar').text(access_value + '%');
      $(node).find('.total_value').text(access_progress + '/' + access_target);

      var node                = $('#completionModal').find('.planning_com');
      $(node).find('.progress-bar').attr('aria-valuenow', planning_value);
      $(node).find('.progress-bar').css('width', planning_value + '%');
      $(node).find('.progress-bar').text(planning_value + '%');
      $(node).find('.total_value').text(planning_progress + '/' + planning_target);

      var node                = $('#completionModal').find('.contract_com');
      $(node).find('.progress-bar').attr('aria-valuenow', contract_value);
      $(node).find('.progress-bar').css('width', contract_value + '%');
      $(node).find('.progress-bar').text(contract_value + '%');
      $(node).find('.total_value').text(contract_progress + '/' + contract_target);

      var node                = $('#completionModal').find('.installation_com');
      $(node).find('.progress-bar').attr('aria-valuenow', installation_value);
      $(node).find('.progress-bar').css('width', installation_value + '%');
      $(node).find('.progress-bar').text(installation_value + '%');
      $(node).find('.total_value').text(installation_progress + '/' + installation_target);

      var node                = $('#completionModal').find('.operation_com');
      $(node).find('.progress-bar').attr('aria-valuenow', operation_value);
      $(node).find('.progress-bar').css('width', operation_value + '%');
      $(node).find('.progress-bar').text(operation_value + '%');
      $(node).find('.total_value').text(operation_progress + '/' + operation_target);

      if(operation_value >= 100)
      {
        $('#completionModal').find('.completion_msg').text("You managed to lead your team to go above and beyond and bring light to over 325,000 hosuelholds! The executive board is enthusiastic, congratulating you for your outstanding results.");
      }
      else if(operation_value >= 90)
      {
        $('#completionModal').find('.completion_msg').text("Despite the fact that you haven't achieved the main target, the executive board recognizes that the leadership team performance is on the right trajectory, retaining your position as CEO.");
      }
      else
      {
        $('#completionModal').find('.completion_msg').text("The negative trajectory of the company in the last two years has scared away critical investors. Too many households are still without electricity, and the executive board is dismissing your from your duties.");
      }
    }
    @if(isset($_GET['action']) && $_GET['action'] == 'play')
      $('#play').trigger('click');
    @endif
    $('#loadTutorial').click(function(){
      $('#tutorialSlider').modal('show');
    });
    $('#loadIntro').click(function(){
      $('#introSlider').modal('show');
    });
    // onCompleteLoad();
	</script>
@endpush