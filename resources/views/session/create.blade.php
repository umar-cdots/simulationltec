@extends('adminlte::page')

@section('title', 'New Session - SIMMETRIC:GAIA')

@section('content_header')
<h1>New Session</h1>
<br>
@endsection

@section('content')
<div class="row">
      <div class="col-md-12">
        <div class="col-md-offset-3 col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Session Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           <form action="{{ route('session.store') }}" method="post">
             @csrf
              <div class="box-body">
                <div class="form-group {{ $errors->has('project_id') ? 'has-error' : ''}}">
                  <label for="name">Project</label>
                  <select class="form-control" id="project_id" name="project_id">
                    <option value="">Select Project</option>
                    @foreach ($projects as $project)
                     <option value="{{$project->id}}" {{ $project->id == old('project_id') ? 'selected' : ''}}>{{$project->title }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('project_id'))
                    <span class="help-block text-danger">{{ $errors->first('project_id') }}</span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                  <label for="phone">Session Title</label>
                  <input type="text" class="form-control" name="title" id="title" placeholder="Enter Session Title" value="{{ old('title') }}">
                  @if ($errors->has('title'))
                    <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        </div>
      </div>
@endsection

@push('js')

@endpush