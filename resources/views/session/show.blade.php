@extends('adminlte::page')

@section('title', 'Session Details - SIMMETRIC:GAIA')

@section('content')
<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="{{ route('session.create') }}"><button class="btn btn-primary add-btn-right">Add New</button></a>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-bordered">
                <tr>
                  <th>Port Name</th>
                  <td>{{ $port_detial->name }}</td>
                </tr>
                <tr>
                  <th>Phone</th>
                  <td>{{ $port_detial->phone }}</td>
                </tr>
                <tr>
                  <th>Email</th>
                  <td>{{ $port_detial->primary_email }}</td>
                </tr>
                 <tr>
                  <th>Address</th>
                  <td>{{ $port_detial->address }}</td>
                </tr>
                <tr>
                  <th>Postal Code</th>
                  <td>{{ $port_detial->postal_code }}</td>
                </tr>
                 <tr>
                  <th>Country</th>
                  <td>{{ $port_detial->country->name }}</td>
                </tr>
                 <tr>
                  <th>State</th>
                  <td>{{ $port_detial->state->name }}</td>
                </tr>
                 <tr>
                  <th>City</th>
                  <td>{{ $port_detial->city->name }}</td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection