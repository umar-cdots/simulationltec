@extends('adminlte::page')

@section('title', 'Sessions - SIMMETRIC:GAIA')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
			<h1 class="pull-left">Sessions</h1>
			<a href="{{ route('session.create') }}">
				<button class="btn btn-primary pull-right">Add New</button>
			</a>
			<div class="clearfix"></div>
		</section>
		<br>
		<section class="content">
			<div class="box">
	            <div class="box-body">
	            	<table class="table table-bordered table-striped" id="table">
	            		<thead>
	            			<tr>
	            				<th>Sr#</th>
	            				<th>Date</th>
	            				<th>Project</th>
	            				<th>Title</th>
	            				<th>Total Users</th>
	            				<th>Action</th>
	            			</tr>
	            		</thead>
	            		<tbody>
	            			@foreach($sessions as $session)
	            				<tr>
	            					<td>{{ $loop->iteration }}</td>
	            					<td>{{ $session->created_at->format('d/m/Y') }}</td>
	            					<td>{{ $session->project->title }}</td>
	            					<td>{{ $session->title }}</td>
	            					<td>{{ $session->users->count() }}</td>
	            					<td>
	            						{{-- <a href="{{ route('session.show', $session->id) }}" class="btn btn-xs btn-success" title="View"><i class="fa fa-eye"></i></a> --}}
		            					{{-- <a href="{{ route('session.edit', $session->id) }}" class="btn btn-xs btn-warning" title="Edit"><i class="fa fa-edit"></i></a> --}}
		            					<form action="{{ route('session.destroy', $session->id) }}" method="POST" style="display: inline;">
										    <input type="hidden" name="_method" value="DELETE">
										    <input type="hidden" name="_token" value="{{ csrf_token() }}">
										    <button type="submit" class="btn btn-xs btn-danger remove" title="Remove"><i class="fa fa-remove"></i></button>
										</form>
	            					</td>
	            				</tr>
	            			@endforeach
	            		</tbody>
	        		</table>
	        	</div>
	        </div>
        </section>
    </div>
@endsection

@push('js')
<script type="text/javascript">
	$('#table').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
</script>
@endpush