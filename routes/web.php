<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'DashboardController@index')->name('simulation')->middleware('auth');
Route::get('/simulator', 'SimulationController@index')->name('simulator')->middleware('user');

Route::get('/performance_analysis/{game_play_id}', 'ReportsController@performanceAnalysis')->name('performance_analysis')->middleware('user'); 
Route::get('/comparative_analysis', 'ReportsController@comparativeAnalysis')->name('comparative_analysis')->middleware('user');
Route::get('/personal_shifts', 'ReportsController@personalShifts')->name('personal_shifts')->middleware('user');
Route::get('/leaderboard', 'ReportsController@leaderboard')->name('leaderboard')->middleware('user');

Route::resource('user', 'UserController')->middleware('super_admin');
Route::get('user/edit/{id}', 'UserController@edit')->name('user.edit')->middleware('auth');
Route::put('user/edit/{id}', 'UserController@update')->name('user.save')->middleware('auth');
Route::resource('session', 'SessionController')->middleware('super_admin');
Route::resource('profile', 'ProfileController')->middleware('auth');

Route::prefix('ajax')->middleware('user')->name('ajax.')->group(function(){
	Route::get('getObservations', 'AjaxController@observation')->name('observation');
	Route::post('startGame', 'AjaxController@startGame')->name('start_game');
	Route::post('endGame', 'AjaxController@endGame')->name('end_game');
	Route::post('nextStage', 'AjaxController@nextStage')->name('next_stage');
	Route::post('observed', 'AjaxController@observed')->name('observed');
	Route::post('engagedAfterObserving', 'AjaxController@engagedAfterObserving')->name('engaged_after_observing');
});